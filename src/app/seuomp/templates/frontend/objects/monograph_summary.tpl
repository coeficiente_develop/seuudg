{**
 * templates/frontend/objects/monograph_summary.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Display a summary view of a monograph for display in lists
 *
 * @uses $monograph Monograph The monograph to be displayed
 * @uses $isFeatured bool Is this a featured monograph?
 *}
<a {if $press}href="{url press=$press->getPath() page="catalog" op="book" path=$monograph->getBestId()}"{else}href="{url page="catalog" op="book" path=$monograph->getBestId()}"{/if} class="text-white">
    <div class="row mb-4 border-bottom pb-2">
      <div class="col-3">
        {assign var="coverImage" value=$monograph->getCurrentPublication()->getLocalizedData('coverImage')}
        <img
				src="{$monograph->getCurrentPublication()->getLocalizedCoverImageThumbnailUrl($monograph->getData('contextId'))}"
				alt="{$coverImage.altText|escape|default:''}"
				class="img-fluid shadow-1-strong rounded"
			>
      </div>

      <div class="col-9">
        <p class="mb-2"><strong>{$monograph->getLocalizedFullTitle()|escape}</strong></p>
        <p>
          <u>{$monograph->getDatePublished()|date_format:$dateFormatLong}</u>
        </p>
        <p>
            <u>{$monograph->getSeriesPosition()|escape}</u>
        </p>
        <p>
            <u>{$monograph->getAuthorOrEditorString()|escape}</u>
        </p>
      </div>
    </div>
</a>

