<div class="container pt-3">
	<div class="row g-2">

        <div class="">
			<div class="bg-light text-center p-2">
				{assign var="coverImage" value=$publication->getLocalizedData('coverImage')}
				<img src="{$publication->getLocalizedCoverImageThumbnailUrl($monograph->getData('contextId'))}"
					alt="{$coverImage.altText|escape|default:''}">
			</div>
		</div>
        
        <div class="">
			<div class="bg-light text-center p-2">
				<h4>Sinopsis/Resumen</h4>
				<p>{$publication->getLocalizedData('abstract')|strip_unsafe_html}</p>
			</div>
		</div>

        <div class="">
			<div class="bg-light text-center p-2">
				<h4>{$publication->getLocalizedFullTitle()|escape}</h4>
				{if $currentPublication->getID() !== $publication->getId()}
				<div class="cmp_notification notice">
					{capture assign="latestVersionUrl"}{url page="catalog" op="book"
					path=$monograph->getBestId()}{/capture}
					{translate key="submission.outdatedVersion"
					datePublished=$publication->getData('datePublished')|date_format:$dateFormatShort
					urlRecentVersion=$latestVersionUrl|escape
					}
				</div>
				{/if}
			</div>
		</div>

		{* <div class="">
		
			{include file="frontend/components/authors.tpl" authors=$publication->getData('authors')}
	
		</div>  *}

        {pluck_files assign=bookFiles files=$availableFiles by="chapter" value=0}
		{if $bookFiles|@count || $remotePublicationFormats|@count}
		<div class="">
			<div class="bg-light text-center p-2">
				<h2>{translate key="submission.downloads"}</h2>
				{include file="frontend/components/publicationFormats.tpl" publicationFiles=$bookFiles}
			</div>
		</div>
		{/if}

        
		
		

		
		{* DOI (requires plugin) *}
		{foreach from=$pubIdPlugins item=pubIdPlugin}
		{if $pubIdPlugin->getPubIdType() != 'doi'}
		{continue}
		{/if}
		{assign var=pubId value=$monograph->getStoredPubId($pubIdPlugin->getPubIdType())}
		{if $pubId}
		{assign var="doiUrl" value=$pubIdPlugin->getResolvingURL($currentPress->getId(), $pubId)|escape}
		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>DOI</h4>
				<a href="{$doiUrl}">
					{$doiUrl}
				</a>
			</div>
		</div>
		{/if}
		{/foreach}


		{if !empty($publication->getLocalizedData('keywords'))}
		<div class="col-12 col-md-6 order-3">
			<div class="bg-light text-center p-2">
				<h4>Keywords</h4>
				<p>{foreach name="keywords" from=$publication->getLocalizedData('keywords') item=keyword}
					{$keyword|escape}{if !$smarty.foreach.keywords.last}, {/if}
					{/foreach}</p>
			</div>
		</div>
		{/if}



		


		{if $chapters|@count }
		<div class="col-12 col-md-6 order-3">
			<div class="bg-light text-center p-2">
				<h4>Capitulos</h4>
				{foreach from=$chapters item=chapter}
				{assign var=chapterId value=$chapter->getId()}
				<li>
					{if $chapter->isPageEnabled()}
					{if $publication->getId() === $currentPublication->getId()}
					<a href="{url page=" catalog" op="book"
						path=$monograph->getBestId()|to_array:"chapter":$chapter->getSourceChapterId()}">
						{else}
						<a href="{url page=" catalog" op="book"
							path=$monograph->getBestId()|to_array:"version":$publication->getId():"chapter":$chapter->getSourceChapterId()}">
							{/if}
							{/if}
							<div class="title">
								{$chapter->getLocalizedTitle()|escape}
								{if $chapter->getLocalizedSubtitle() != ''}
								<div class="subtitle">
									{$chapter->getLocalizedSubtitle()|escape}
								</div>
								{/if}
							</div>
							{if $chapter->isPageEnabled()}
						</a>
						{/if}
						{assign var=chapterAuthors value=$chapter->getAuthorNamesAsString()}
						{if $authorString != $chapterAuthors}
						<div class="authors">
							{$chapterAuthors|escape}
						</div>
						{/if}

						{* DOI (requires plugin) *}
						{foreach from=$pubIdPlugins item=pubIdPlugin}
						{if $pubIdPlugin->getPubIdType() != 'doi'}
						{continue}
						{/if}
						{assign var=pubId value=$chapter->getStoredPubId($pubIdPlugin->getPubIdType())}
						{if $pubId}
						{assign var="doiUrl" value=$pubIdPlugin->getResolvingURL($currentPress->getId(), $pubId)|escape}
						<div class="doi">{translate key="plugins.pubIds.doi.readerDisplayName"} <a
								href="{$doiUrl}">{$doiUrl}</a></div>
						{/if}
						{/foreach}

						{* Display any files that are assigned to this chapter *}
						{pluck_files assign="chapterFiles" files=$availableFiles by="chapter" value=$chapterId}
						{if $chapterFiles|@count}
						<div class="files">

							{* Display chapter files sorted by publication format so that they are ordered
							consistently across all chapters. *}
							{foreach from=$publicationFormats item=format}
							{pluck_files assign="pubFormatFiles" files=$chapterFiles by="publicationFormat"
							value=$format->getId()}

							{foreach from=$pubFormatFiles item=file}

							{* Use the publication format name in the download link unless a pub format has multiple
							files *}
							{assign var=useFileName value=false}
							{if $pubFormatFiles|@count > 1}
							{assign var=useFileName value=true}
							{/if}

							{include file="frontend/components/downloadLink.tpl" downloadFile=$file monograph=$monograph
							publicationFormat=$format currency=$currency useFilename=$useFileName}
							{/foreach}
							{/foreach}
						</div>
						{/if}
				</li>
				{/foreach}
			</div>
		</div>
		{/if}



		<div class="col-12 col-md-6 order-4">
			<div class="bg-light text-center p-2">
				<h4>Biografias</h4>
				{assign var="hasBiographies" value=0}
				{foreach from=$publication->getData('authors') item=author}
				{if $author->getLocalizedBiography()}
				{assign var="hasBiographies" value=$hasBiographies+1}
				{/if}
				{/foreach}
				{if $hasBiographies}
				<div class="item author_bios">
					<h2 class="label">
						{if $hasBiographies > 1}
						{translate key="submission.authorBiographies"}
						{else}
						{translate key="submission.authorBiography"}
						{/if}
					</h2>
					{foreach from=$publication->getData('authors') item=author}
					{if $author->getLocalizedBiography()}
					<div class="sub_item">
						<div class="label">
							{if $author->getLocalizedAffiliation()}
							{capture assign="authorName"}{$author->getFullName()|escape}{/capture}
							{capture assign="authorAffiliation"}<span
								class="affiliation">{$author->getLocalizedAffiliation()|escape}</span>{/capture}
							{translate key="submission.authorWithAffiliation" name=$authorName
							affiliation=$authorAffiliation}
							{else}
							{$author->getFullName()|escape}
							{/if}
						</div>
						<div class="value">
							{$author->getLocalizedBiography()|strip_unsafe_html}
						</div>
					</div>
					{/if}
					{/foreach}
				</div>
				{/if}
			</div>
		</div>

		<div class="col-12 col-md-6 order-4">
			<div class="bg-light text-center p-2">
				<h4>Referencias</h4>

				{if $citations || $publication->getData('citationsRaw')}
				<div class="item references">
					<h2 class="label">
						{translate key="submission.citations"}
					</h2>
					<div class="value">
						{if $citations}
						{foreach from=$citations item=$citation}
						<p>{$citation->getCitationWithLinks()|strip_unsafe_html}</p>
						{/foreach}
						{else}
						{$publication->getData('citationsRaw')|escape|nl2br}
						{/if}
					</div>
				</div>
				{/if}
			</div>
		</div>








		<div class="col-12 col-md-6 order-5">
			<div class="bg-light text-center p-2">
				<h4>Formatos de Publicacion</h4>
				{* Publication formats *}
				{if count($publicationFormats)}
				{foreach from=$publicationFormats item="publicationFormat"}
				{if $publicationFormat->getIsApproved()}

				{assign var=identificationCodes value=$publicationFormat->getIdentificationCodes()}
				{assign var=identificationCodes value=$identificationCodes->toArray()}
				{assign var=publicationDates value=$publicationFormat->getPublicationDates()}
				{assign var=publicationDates value=$publicationDates->toArray()}
				{assign var=hasPubId value=false}
				{foreach from=$pubIdPlugins item=pubIdPlugin}
				{assign var=pubIdType value=$pubIdPlugin->getPubIdType()}
				{if $publicationFormat->getStoredPubId($pubIdType)}
				{assign var=hasPubId value=true}
				{break}
				{/if}
				{/foreach}

				{* Skip if we don't have any information to print about this pub format *}
				{if !$identificationCodes && !$publicationDates && !$hasPubId &&
				!$publicationFormat->getPhysicalFormat()}
				{continue}
				{/if}

				<div class="item publication_format">

					{* Only add the format-specific heading if multiple publication formats exist *}
					{if count($publicationFormats) > 1}
					<h2 class="pkp_screen_reader">
						{assign var=publicationFormatName value=$publicationFormat->getLocalizedName()}
						{translate key="monograph.publicationFormatDetails" format=$publicationFormatName|escape}
					</h2>

					<div class="sub_item item_heading format">
						<div class="label">
							{$publicationFormat->getLocalizedName()|escape}
						</div>
					</div>
					{else}
					<h2 class="pkp_screen_reader">
						{translate key="monograph.miscellaneousDetails"}
					</h2>
					{/if}


					{* DOI's and other identification codes *}
					{if $identificationCodes}
					{foreach from=$identificationCodes item=identificationCode}
					<div class="sub_item identification_code">
						<h3 class="label">
							{$identificationCode->getNameForONIXCode()|escape}
						</h3>
						<div class="value">
							{$identificationCode->getValue()|escape}
						</div>
					</div>
					{/foreach}
					{/if}

					{* Dates of publication *}
					{if $publicationDates}
					{foreach from=$publicationDates item=publicationDate}
					<div class="sub_item date">
						<h3 class="label">
							{$publicationDate->getNameForONIXCode()|escape}
						</h3>
						<div class="value">
							{assign var=dates value=$publicationDate->getReadableDates()}
							{* note: these dates have dateFormatShort applied to them in getReadableDates() if they need
							it *}
							{if $publicationDate->isFreeText() || $dates|@count == 1}
							{$dates[0]|escape}
							{else}
							{* @todo the &mdash; ought to be translateable *}
							{$dates[0]|escape}&mdash;{$dates[1]|escape}
							{/if}
							{if $publicationDate->isHijriCalendar()}
							<div class="hijri">
								{translate key="common.dateHijri"}
							</div>
							{/if}
						</div>
					</div>
					{/foreach}
					{/if}

					{* PubIDs *}
					{foreach from=$pubIdPlugins item=pubIdPlugin}
					{assign var=pubIdType value=$pubIdPlugin->getPubIdType()}
					{assign var=storedPubId value=$publicationFormat->getStoredPubId($pubIdType)}
					{if $storedPubId != ''}
					<div class="sub_item pubid {$publicationFormat->getId()|escape}">
						<h2 class="label">
							{$pubIdType}
						</h2>
						<div class="value">
							{$storedPubId|escape}
						</div>
					</div>
					{/if}
					{/foreach}

					{* Physical dimensions *}
					{if $publicationFormat->getPhysicalFormat()}
					<div class="sub_item dimensions">
						<h2 class="label">
							{translate key="monograph.publicationFormat.productDimensions"}
						</h2>
						<div class="value">
							{$publicationFormat->getDimensions()|escape}
						</div>
					</div>
					{/if}
				</div>
				{/if}
				{/foreach}
				{/if}

			</div>
		</div>


		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>{translate key="series.series"}</h4>
				<h6><button class="btn btn-info" href="{url page=" catalog" op="series" path=$series->getPath()}">
						{$series->getLocalizedFullTitle()|escape}
					</button></h6>
				{if $series->getOnlineISSN()}
				<h6>{$series->getOnlineISSN()|escape}</h6>
				{/if}
				{if $series->getPrintISSN()}
				<h6>{$series->getPrintISSN()|escape}</h6>
				{/if}
			</div>
		</div>

		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>Categorias</h4>
				{foreach from=$categories item="category"}

				<button class="btn btn-success" href="{url op=" category" path=$category->getPath()}">
					{$category->getLocalizedTitle()|strip_unsafe_html}
				</button>

				{/foreach}
			</div>
		</div>

		{if $publication->getData('licenseUrl')}
		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>Licencia</h4>
				{if $ccLicenseBadge}
				{$ccLicenseBadge}
				{else}
				<a href="{$publication->getData('licenseUrl')|escape}">
					{translate key="submission.license"}
				</a>
				{/if}
			</div>
		</div>
		{/if}



		{if $publication->getData('copyrightYear') && $publication->getLocalizedData('copyrightHolder')}
		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>Copyright</h4>
				{translate|escape key="submission.copyrightStatement"
				copyrightYear=$publication->getData('copyrightYear')
				copyrightHolder=$publication->getLocalizedData('copyrightHolder')}
			</div>
		</div>
		{/if}







	</div>
</div>