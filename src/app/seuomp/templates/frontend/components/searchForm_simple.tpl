{**
* templates/frontend/components/searchForm_simple.tpl
*
* Copyright (c) 2014-2021 Simon Fraser University
* Copyright (c) 2003-2021 John Willinsky
* Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
*
* @brief Simple display of a search form with just text input and search button
*
* @uses $searchQuery string Previously input search query
*}


{* <form class="d-flex">
  <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
  <button class="btn btn-danger" type="submit"  style="width:80px;height:48px; margin-top:13px">Search</button>
</form> *}



{* <div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username"
    aria-describedby="basic-addon2">
  <div class="input-group-append">
    <span class="input-group-text" id="basic-addon2">@example.com</span>
  </div>
</div> *}


{if !$currentJournal || $currentJournal->getData('publishingMode') != $smarty.const.PUBLISHING_MODE_NONE}
{capture name="searchFormUrl"}{url page="search" op="search" escape=false}{/capture}
{assign var=formUrlParameters value=[]}{* Prevent Smarty warning *}
{$smarty.capture.searchFormUrl|parse_url:$smarty.const.PHP_URL_QUERY|parse_str:$formUrlParameters}

<form class="d-flex" action="{$smarty.capture.searchFormUrl|strtok:" ?"|escape}" method="get"
  role="search" aria-label="{translate|escape key=" submission.search"}">


  
          {foreach from=$formUrlParameters key=paramKey item=paramValue}
          <input type="hidden" name="{$paramKey|escape}" value="{$paramValue|escape}" />
          {/foreach}
          {block name=searchQuerySimple}
          <input name="query" value="{$searchQuery|escape}" aria-label="{translate|escape key=" common.searchQuery"}"
            type="text" class="form-control me-8">
          <button class="btn btn-danger" type="submit"  style="width:80px;height:48px; margin-top:13px">{translate key="common.search"}</button>
          
          {/block}
       
  


</form>

{/if}
