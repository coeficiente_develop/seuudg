
<div class="bg-light text-center p-2">
{* Only show editors for edited volumes *}
	{if $monograph->getWorkType() == $monograph::WORK_TYPE_EDITED_VOLUME && $editors|@count && !$isChapterRequest}
		{assign var="authors" value=$editors}
		{assign var="identifyAsEditors" value=true}
	{/if}

                    <h4>Autores</h4>
                    {foreach name="authors" from=$authors item=author}
						
						{if $identifyAsEditors}
						<h6>{translate key="submission.editorName" editorName=$author->getFullName()|escape}</h6>
					    {else}
						<h6>{$author->getFullName()|escape}<h6>
						<h6>{$author->getLocalizedAffiliation()|escape}<h6>
						<h6>{$author->getOrcid()|escape}<h6>
					    {/if}
					{/foreach}                   
</div>
