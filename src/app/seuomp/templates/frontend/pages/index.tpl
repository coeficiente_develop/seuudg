{**
 * templates/frontend/pages/index.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Display the front page of the site
 *
 * @uses $homepageImage array Details about the uploaded homepage image
 * @uses $spotlights array Selected spotlights to promote on the homepage
 * @uses $featuredMonographs array List of featured releases in this press
 * @uses $newReleases array List of new releases in this press
 * @uses $announcements array List of announcements
 * @uses $numAnnouncementsHomepage int Number of announcements to display on the
 *       homepage
 * @uses $additionalHomeContent string HTML blob of arbitrary content added by
 *  an editor/admin.
 *}
{include file="frontend/components/header.tpl"}


        <!-- Masthead-->
        <header class="masthead">
            <div class="container position-relative">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <div class="text-center text-white">
                            <!-- Page heading-->
                            <h1 class="mb-5">Sistema Editorial Universitario</h1>
                            {* <button class="btn btn-primary">Entrar</button> *}
                            <!-- Signup form-->
                            <!-- * * * * * * * * * * * * * * *-->
                            <!-- * * SB Forms Contact Form * *-->
                            <!-- * * * * * * * * * * * * * * *-->
                            <!-- This form is pre-integrated with SB Forms.-->
                            <!-- To make this form functional, sign up at-->
                            <!-- https://startbootstrap.com/solution/contact-forms-->
                            <!-- to get an API token!-->
                            
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Icons Grid-->
        
        <section class="features-icons bg-light text-center">
            <h2>Publica con UDG</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-window m-auto text-primary"></i></div>
                            
                            <h3>
                            <a class="text-dark" href="">¿Como publicar en UDG?</a>
                            </h3>
                            <p class="lead mb-0">Ven y publica con nosotros</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-file-earmark-check m-auto text-primary"></i></div>
                            <h3>
                            <a class="text-dark" href="{url page="about" op="submissions"}">Requisitos y Normatividad</a>
                            
                            </h3>
                            <p class="lead mb-0">Por favor lea los requisitos antes de solicitar</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-file-text m-auto text-primary"></i></div>
                            <h3>
                            <a class="text-dark" href="{url page="about" op="privacy"}">Aviso de Privacidad</a>
                            </h3>
                            <p class="lead mb-0">Por favor consulte el aviso de privacidad</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-chat-right-text-fill m-auto text-primary"></i></div>
                            <h3>
                            <a class="text-dark" href="{url page="about" op="contact"}">Contactanos</a>
                            </h3>
                            <p class="lead mb-0">Ponte en contacto con nosotros</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="features-icons bg-light text-center">
            <h2>Sitios de Interes</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-gear-wide-connected m-auto text-primary"></i></div>
                            <h6>COORDINACION GENERAL DE INVESTIGACION,POSGRADO Y VINCULACION</h6>
                            <!-- <p class="lead mb-0">Ven y publica con nosotros</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-bank m-auto text-primary"></i></div>
                            <h5>
                            <a class="text-dark" href="https://www.udg.mx/">UNIVERSIDAD DE GUADALAJARA</a>
                            
                            </h5>
                            <!-- <p class="lead mb-0">Por favor lea los requisitos antes de solicitar</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-journals m-auto text-primary"></i></div>
                            <h5>
                            <a class="text-dark" href="{url page="catalog"}">
                            Catalogo
                            </a>
                            </h5>
                            <!-- <p class="lead mb-0">Por favor consulte el reglamento editorial</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>


{include file="frontend/components/footer.tpl"}
