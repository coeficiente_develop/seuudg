{**
 * templates/frontend/pages/catalog.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Display the page to view the catalog.
 *
 * @uses $publishedSubmissions array List of published submissions
 * @uses $contextSeries array List of context series
 * @uses $prevPage int The previous page number
 * @uses $nextPage int The next page number
 * @uses $showingStart int The number of the first item on this page
 * @uses $showingEnd int The number of the last item on this page
 * @uses $total int Count of all published submissions
 *}
{include file="frontend/components/header.tpl" pageTitle="navigation.catalog"}


<div class="card">
  <div class="card-header">
    {translate key="navigation.catalog"}
  </div>
  <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p>{translate key="catalog.browseTitles" numTitles=$total}</p>
      {* <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer> *}
    </blockquote>
  </div>
</div>	



<main class="my-5">
    
   

    <div class="container">

    

	{include file="frontend/components/monographList.tpl" monographs=$publishedSubmissions featured=$featuredMonographIds}
     
    </div>
  </main>


{include file="frontend/components/footer.tpl"}
