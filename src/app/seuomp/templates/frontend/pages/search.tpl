{**
 * templates/frontend/pages/search.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Display the page to search and view search results.
 *
 * @uses $results array List of search results
 * @uses $searchQuery string The search query, if one was just made
 *}
{include file="frontend/components/header.tpl" pageTitle="common.search"}

<br>

    <div class="row">
          <div class="col-sm-12">
            <center>
              <img src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/UDG640x640.png" style="width:80px;height:100px;" width="100">
            </center>
          </div>
    </div>
  	<div class="row">
      <div class="col-sm-12">
        <h1  class="text-center" style="color:White;">{translate key="common.search"}</h1>
      </div>
    </div>
    <div class="row"><br> </div>
    <a name="search-form"></a>
    {include file="frontend/components/searchForm_simple.tpl"}
    <div class="row"><br><br> <hr style="color:White;"> </div>
    <div class="row">
      <div class="col-sm-12">
  		   <h4 class="text-center" style="color:White;">
            {translate key="catalog.browseTitles" numTitles=$results->getCount()}
          </h4>
      </div>
  	</div>
    <div class="row"><br></div>

  	{if $searchQuery == '' }


  	{elseif $results->getCount() == 0}
    <div class="row">
      <div class="col-sm-12">
         <h4 class="text-center" style="color:White;">
           	{translate key="catalog.noTitlesSearch" searchQuery=$searchQuery|escape}
          </h4>
      </div>
    </div>
    <div class="row"> <br> <hr></div>
  	{else}
    <div class="row">
      <div class="col-sm-12">
         <div class="text-center" style="color:White;">
           <div>
             {if $results->getCount() > 1}
               {translate key="catalog.foundTitlesSearch" searchQuery=$searchQuery|escape number=$results->getCount()}
             {else}
               {translate key="catalog.foundTitleSearch" searchQuery=$searchQuery|escape}
             {/if}
             <a href="#search-form">
               {translate key="search.searchAgain"}
             </a>
           </div>
           <div>
             {assign var=counter value=1}
             {iterate from=results item=result}
               {if $counter is odd by 1}
                 <div >
               {/if}
                 {include file="frontend/objects/monograph_summary.tpl" monograph=$result.publishedSubmission press=$result.press heading="h2"}
               {if $counter is even by 1}
                 </div>
               {/if}
               {assign var=counter value=$counter+1}
             {/iterate}
             {* Close .row if we have an odd number of titles *}
             {if $counter > 1 && $counter is even by 1}
               </div>
             {/if}
             <div class="">
               {page_info iterator=$results}
               {page_links anchor="results" iterator=$results name="search" query=$searchQuery}
             </div>

           </div>
         </div>
      </div>
    
    

  	{/if}

{include file="frontend/components/footer.tpl"}
