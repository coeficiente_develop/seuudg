{**
 * lib/pkp/templates/frontend/components/header.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Common frontend site header.
 *
 * @uses $isFullWidth bool Should this page be displayed without sidebars? This
 *       represents a page-level override, and doesn't indicate whether or not
 *       sidebars have been configured for thesite.
 *}

<!DOCTYPE html>
<html lang="{$currentLocale|replace:"_":"-"}" xml:lang="{$currentLocale|replace:"_":"-"}">
{if !$pageTitleTranslated}{capture assign="pageTitleTranslated"}{translate key=$pageTitle}{/capture}{/if}
{include file="frontend/components/headerHead.tpl"}
<body class="pkp_page_{$requestedPage|escape|default:"index"} pkp_op_{$requestedOp|escape|default:"index"}{if $showingLogo} has_site_logo{/if}" dir="{$currentLocaleLangDir|escape|default:"ltr"}">


    <div class="hero-image">
	    
    <div class="hero-text">
        {* <img id="imgbanner" class="img-fluid" src="http://192.168.64.2/seuomp/udg_resources/assets/img/logo_udg_sf.png" style="width:80px;height:80px; margin-left:-1200px;"/> *}
        <img id="imgbanner" class="img-fluid" src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/UDG640x640.png" style="width:80px;height:95px; margin-left:-1150px;"/>
        
        <p style="margin-left:-100px;">SISTEMA EDITORIAL UNIVERSITARIO (SEU)</p>
    </div>
  </div>

	

		{* Header *}
		
			

				{capture assign="primaryMenu"}
					{load_menu name="primary" id="navigationPrimary" ulClass="pkp_navigation_primary"}
				{/capture}



                  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #6B33FF;">
      <div class="container-fluid">
	  {$primaryMenu}
	  {* {if $currentContext && $requestedPage !== 'search'}
								
									<a href="{url page="search"}" class="nav-link">
										<span class="bi bi-search" aria-hidden="true"></span>
										  {translate key="common.search"}
									</a>
							
							{/if} *}
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            {* <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">ACERCA DE</a>
            </li> *}
            <li class="nav-item">
              {* <a class="nav-link" href="{url page="login"}">Entrar</a> *}
            </li>
            <li class="nav-item dropdown">
              {* <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                SEDE
              </a> *}
              {* <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Something else here</a></li>
              </ul> *}
            </li>
            <li class="nav-item">
			{load_menu name="user" id="navigationUser" ulClass="pkp_navigation_user" liClass="profile"}
              {* <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Contacto</a> *}
            </li>
            <li class="nav-item">
              {* <a class="nav-link active" aria-current="page" href="#">Buscar</a> *}
            </li>
            <li class="nav-item">
              {* <a class="nav-link active" aria-current="page" href="#">Entrar</a> *}
            </li>
            <li class="nav-item">
              {* <a class="nav-link active" aria-current="page" href="#">Mi Cuenta</a> *}
            </li>
          </ul>
        </div>
      </div>
    </nav>

				

				
		
		
			