{**
 * templates/frontend/components/footer.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Common site frontend footer.
 *
 * @uses $isFullWidth bool Should this page be displayed without sidebars? This
 *       represents a page-level override, and doesn't indicate whether or not
 *       sidebars have been configured for thesite.
 *}

	

	




		<footer class="bg-dark text-white text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">

          {* <img class="img-fluid" id="imgfooter" src="http://192.168.64.2/seuomp/udg_resources/assets/img/logo_udg_sf.png" style="width:80px;height:80px;" class="img-fluid" style="width:100px;height:100px;"> *}
          <img class="img-fluid" id="imgfooter" src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/UDG640x640.png" style="width:80px;height:100px;" class="img-fluid">

          <!-- <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul> -->
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">


          <h6>Universidad de Guadalajara
            <br>
            <br>
            Av.Juarez No.976
            <br>
             Colonia Centro C.P 44100 Guadalajara, Jalisco, Mexico
             <br>
            Telefono +52 (33) 3134 2297
            <br>
             Ext 12297
          </h6>

          <!-- <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul> -->
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          

          {* <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Inicio</a>
            </li>
            <li>
              <a href="#!" class="text-white">Directorio</a>
            </li>
            <li>
              <a href="#!" class="text-white">Noticias</a>
            </li>
            <li>
              <a href="#!" class="text-white">Agenda</a>
            </li>
            <li>
                <a href="#!" class="text-white">Contacto</a>
              </li>
          </ul> *}
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-white text-uppercase mb-0">Redes Sociales</h5>

          <!-- Facebook -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #3b5998;"
href="https://www.facebook.com/udg.mx"
role="button"
><i class="bi bi-facebook m-auto"></i
></a>

<!-- Twitter -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #55acee;"
href="https://twitter.com/udg_oficial"
role="button"
><i class="bi bi-twitter m-auto"></i
></a>



<!-- Instagram -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #ac2bac;"
href="https://www.instagram.com/udg_oficial/"
role="button"
><i class="bi bi-instagram m-auto"></i
></a>

<!-- Linkedin -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #0082ca;"
href="https://www.linkedin.com/school/universidad-de-guadalajara_2/"
role="button"
><i class="bi-linkedin m-auto"></i
></a>

        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->

          

    <div class="text-center text-white p-3" style="background-color: rgba(0, 0, 0, 0.2);">
      
     
          <ul class="list-unstyled mb-0">
            <li>
              <h5 class="text-white"> Derechos reservados©2022</h5>
            </li>
            <li>
              {* <a href="https://www.coeficiente.mx/" class="text-white">Universidad de Guadalajara</a> *}
            </li>
            <li>
              <a href="https://www.coeficiente.mx/" class="text-white">COEFICIENTE</a>
            </li>
            
          </ul>
    </div>
    <!-- Copyright -->
  </footer>

		
	




{load_script context="frontend"}


</body>
</html>
