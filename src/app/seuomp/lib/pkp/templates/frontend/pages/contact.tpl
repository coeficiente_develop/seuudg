{**
 * templates/frontend/pages/contact.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Display the page to view the press's contact details.
 *
 * @uses $currentContext Journal|Press The current journal or press
 * @uses $mailingAddress string Mailing address for the journal/press
 * @uses $contactName string Primary contact name
 * @uses $contactTitle string Primary contact title
 * @uses $contactAffiliation string Primary contact affiliation
 * @uses $contactPhone string Primary contact phone number
 * @uses $contactEmail string Primary contact email address
 * @uses $supportName string Support contact name
 * @uses $supportPhone string Support contact phone number
 * @uses $supportEmail string Support contact email address
 *}
{include file="frontend/components/header.tpl" pageTitle="about.contact"}

<header class="masthead">
    <div class="container position-relative">
        <div class="row justify-content-center">
            <div class="col-xl-6">
                <div class="text-center text-white">
                    <!-- Page heading-->
                    <h1 class="mb-5">Sistema Editorial Universitario</h1>
                    <button class="btn btn-primary">Conoce nuestros lineamientos</button>
                    <!-- Signup form-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- * * SB Forms Contact Form * *-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- This form is pre-integrated with SB Forms.-->
                    <!-- To make this form functional, sign up at-->
                    <!-- https://startbootstrap.com/solution/contact-forms-->
                    <!-- to get an API token!-->
                    
                </div>
            </div>
        </div>
    </div>
</header>

<section class="features-icons bg-dark text-center text-white">
    <h1>{translate key="about.contact"}</h1>
    <div class="container">
        <div class="row">
            <h6>
			{$mailingAddress|nl2br|strip_unsafe_html}
			</h6>
			<h6>
			{$contactName|escape}
			</h6>
			<h6>
			{$contactTitle|escape}
			</h6>
			<h6>
			{$contactAffiliation|strip_unsafe_html}
			</h6>
			<h6>
			{$contactPhone|escape}
			</h6>
			<h6>
			{mailto address=$contactEmail encode='javascript'}
			</h6>

			<h2>Soporte Tecnico</h2>
			<h6>{$supportName|escape}</h6>
			<h6>{$supportEmail|escape}</h6>
			<h6>{$supportPhone|escape}</h6>
			
			
			
			
			
        </div>
    </div>
</section>

<section class="features-icons bg-light text-center">
    <h2>Sitios de Interes</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-gear-wide-connected m-auto text-primary"></i></div>
                    <h6>COORDINACION GENERAL DE INVESTIGACION,POSGRADO Y VINCULACION</h6>
                    <!-- <p class="lead mb-0">Ven y publica con nosotros</p> -->
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-bank m-auto text-primary"></i></div>
                    <h5>UNIVERSIDAD DE GUADALAJARA</h5>
                    <!-- <p class="lead mb-0">Por favor lea los requisitos antes de solicitar</p> -->
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-journals m-auto text-primary"></i></div>
                    <h5>EDITORIAL UNIVERSIDAD DE GUADALAJARA</h5>
                    <!-- <p class="lead mb-0">Por favor consulte el reglamento editorial</p> -->
                </div>
            </div>
        </div>
    </div>
</section>


{include file="frontend/components/footer.tpl"}
