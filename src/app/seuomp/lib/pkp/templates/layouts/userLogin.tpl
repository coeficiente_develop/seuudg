{**
 * templates/frontend/pages/userLogin.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2000-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * User login form.
 *
 *}
{include file="frontend/components/header.tpl" pageTitle="user.login"}


    <div class="container mt-5 mb-5">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-6">
                <div class="card bg-dark p-3 text-center text-white">
				
				

				<input type="hidden" name="source" value="{$source|escape}" />
                    
					<form id="login" method="post" action="{$loginUrl}">
                    {csrf}
                    
                    
                    <div> <img src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/logo_udg_sf.png" width="100"> </div>
                    {if $error}
               
                   {translate key=$error reason=$reason}
               
                {/if}

				{if $loginMessage}
    		<h6>
    			{translate key=$loginMessage}
    		</h6>
    	        {/if}
                    <h1>Bienvenido</h1>
                    <div class="p-2 px-5"> 
                        <input class="form-control" type="text" name="username" id="username" value="{$username|escape}" maxlength="32" required aria-required="true" autocomplete="username" placeholder="Usuario">
                        <input type="password" class="form-control" name="password" id="password" value="{$password|escape}" password="true" maxlength="32" required aria-required="true" autocomplete="current-password" placeholder="Contraseña">
                        <hr>
                         <button class="btn btn-danger w-100 signup-button" type="submit">Iniciar Sesion</button>
						 <hr>
                         <label><input type="checkbox" name="remember" id="remember" value="1" checked="$remember"> Mantenerse Conectado</label><br>
                        
                        <hr> 
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

                    </form>
    
	{* recaptcha spam blocker *}
        			{if $recaptchaPublicKey}
        				<fieldset class="recaptcha_wrapper">
        					<div class="fields">
        						<div class="recaptcha">
        							<div class="g-recaptcha" data-sitekey="{$recaptchaPublicKey|escape}">
        							</div>
        						</div>
        					</div>
        				</fieldset>
        			{/if}

{include file="frontend/components/footer.tpl"}
