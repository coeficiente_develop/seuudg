{**
 * lib/pkp/templates/stats/users.tpl
 *
 * Copyright (c) 2013-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * The editorial statistics page.
 *
 *}
{extends file="layouts/backend.tpl"}

{block name="page"}
	<div class="">
		<div class="row">
			<div class="col-sm-9">
				<div id="usersTableLabel" class="text-center h2">
					<strong>
						{translate key="manager.statistics.statistics.registeredUsers"}
					</strong>
				</div>				
			</div>
			<div class="col-sm-3">
				<button type="button" class="form-control btn btn-outline-primary"  data-bs-toggle="modal" data-bs-target="#exampleModal" ref="exportButton" @click="$modal.show('export')">
					{translate key="common.export"}
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<table class="table" labelled-by="usersTableLabel">
					<thead>
						<tr>
							<th>{translate key="common.name"}</th>
							<th>{translate key="stats.total"}</th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$userStats item=$row}
							<tr>
								<td>{$row.name}</td>
								<td>{$row.value}</td>
							</tr>
						{/foreach}
					</tbody>
				</table>
			</div>
		</div>
			
			
		</div>
	</div>
	{*
	<modal
		v-bind="MODAL_PROPS"
		name="export"
		@closed="setFocusToRef('exportButton')"
	>
		<modal-content
			close-label="common.close"
			modal-name="export"
			title="{translate key="manager.export.usersToCsv.label"}"
		>
			
		</modal-content>
	</modal>
	*}

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			
				<div class="modal-body">
					<pkp-form v-bind="components.usersReportForm" @set="set" @success="loadExport" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					
				</div>
			</div>
		</div>
	</div>
{/block}
