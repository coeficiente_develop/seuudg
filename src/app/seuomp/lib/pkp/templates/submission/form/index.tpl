{**
 * templates/submission/form/index.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * Main template for the author's submission pages.
 *}
{extends file="layouts/backend.tpl"}

{block name="page"}
	<h1 class="text-center h2 titulo-pkp-">
		<strong>  {translate key="submission.submit.title"}
	</h1> </strong>

	<script type="text/javascript">
		// Attach the JS file tab handler.
		$(function() {ldelim}
			$('#submitTabs').pkpHandler(
				'$.pkp.pages.submission.SubmissionTabHandler',
				{ldelim}
					submissionProgress: {$submissionProgress},
					selected: {$submissionProgress-1},
					cancelUrl: {url|json_encode page="submissions" escape=false},
					cancelConfirmText: {translate|json_encode key="submission.submit.cancelSubmission"}
				{rdelim}
			);
		{rdelim});
	</script>
	{if $currentContext->getData('disableSubmissions')}
		<notification>
			{translate key="manager.setup.disableSubmissions.notAccepting"}
		</notification>
	{else}
		<div id="submitTabs" class="container-fluid -pk bg-white">
			<ul>
				{foreach from=$steps key=step item=stepLocaleKey}
					<li><a class="btn btn-outline-primary" name="step-{$step|escape}" href="{url op="step" path=$step submissionId=$submissionId sectionId=$sectionId}">{$step}. {translate key=$stepLocaleKey}</a></li>
				{/foreach}
			</ul>
		</div>
	{/if}
{/block}
