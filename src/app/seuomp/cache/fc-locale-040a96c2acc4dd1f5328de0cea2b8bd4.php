<?php return array (
  'emails.notification.subject' => 'New notification from {$siteTitle}',
  'emails.notification.body' => 'You have a new notification from {$siteTitle}:<br />
<br />
{$notificationContents}<br />
<br />
Link: {$url}<br />
<br />
This is an automatically generated email; please do not reply to this message.<br />
{$principalContactSignature}',
  'emails.notification.description' => 'The email is sent to registered users that have selected to have this type of notification emailed to them.',
  'emails.passwordResetConfirm.subject' => 'Password Reset Confirmation',
  'emails.passwordResetConfirm.body' => 'We have received a request to reset your password for the {$siteTitle} web site.<br />
<br />
If you did not make this request, please ignore this email and your password will not be changed. If you wish to reset your password, click on the below URL.<br />
<br />
Reset my password: {$url}<br />
<br />
{$principalContactSignature}',
  'emails.passwordResetConfirm.description' => 'This email is sent to a registered user when they indicate that they have forgotten their password or are unable to login. It provides a URL they can follow to reset their password.',
  'emails.passwordReset.subject' => 'Password Reset',
  'emails.passwordReset.body' => 'Your password has been successfully reset for use with the {$siteTitle} web site.<br />
<br />
Your username: {$username}<br />
Your new password: {$password}<br />
<br />
{$principalContactSignature}',
  'emails.passwordReset.description' => 'This email is sent to a registered user when they have successfully reset their password following the process described in the PASSWORD_RESET_CONFIRM email.',
  'emails.userRegister.subject' => 'Press Registration',
  'emails.userRegister.body' => '{$userFullName}<br />
<br />
You have now been registered as a user with {$contextName}. We have included your username and password in this email, which are needed for all work with this press through its website. At any point, you can ask to be removed from the list of users by contacting me.<br />
<br />
Username: {$username}<br />
Password: {$password}<br />
<br />
Thank you,<br />
{$principalContactSignature}',
  'emails.userRegister.description' => 'This email is sent to a newly registered user to welcome them to the system and provide them with a record of their username and password.',
  'emails.userValidate.subject' => 'Validate Your Account',
  'emails.userValidate.body' => '{$userFullName}<br />
<br />
You have created an account with {$contextName}, but before you can start using it, you need to validate your email account. To do this, simply follow the link below:<br />
<br />
{$activateUrl}<br />
<br />
Thank you,<br />
{$principalContactSignature}',
  'emails.userValidate.description' => 'This email is sent to a newly registered user to welcome them to the system and provide them with a record of their username and password.',
  'emails.reviewerRegister.subject' => 'Registration as Reviewer with {$contextName}',
  'emails.reviewerRegister.body' => 'In light of your expertise, we have taken the liberty of registering your name in the reviewer database for {$contextName}. This does not entail any form of commitment on your part, but simply enables us to approach you with a submission to possibly review. On being invited to review, you will have an opportunity to see the title and abstract of the paper in question, and you\'ll always be in a position to accept or decline the invitation. You can also ask at any point to have your name removed from this reviewer list.<br />
<br />
We are providing you with a username and password, which is used in all interactions with the press through its website. You may wish, for example, to update your profile, including your reviewing interests.<br />
<br />
Username: {$username}<br />
Password: {$password}<br />
<br />
Thank you,<br />
{$principalContactSignature}',
  'emails.reviewerRegister.description' => 'This email is sent to a newly registered reviewer to welcome them to the system and provide them with a record of their username and password.',
  'emails.publishNotify.subject' => 'New Book Published',
  'emails.publishNotify.body' => 'Readers:<br />
<br />
{$contextName} has just published its latest book at {$contextUrl}. We invite you to review the Table of Contents here and then visit our web site to review monographs and items of interest.<br />
<br />
Thanks for the continuing interest in our work,<br />
{$editorialContactSignature}',
  'emails.publishNotify.description' => 'This email is sent to registered readers via the "Notify Users" link in the Editor\'s User Home. It notifies readers of a new book and invites them to visit the press at a supplied URL.',
  'emails.submissionAck.subject' => 'Submission Acknowledgement',
  'emails.submissionAck.body' => '{$authorName}:<br />
<br />
Thank you for submitting the manuscript, &quot;{$submissionTitle}&quot; to {$contextName}. With the online press management system that we are using, you will be able to track its progress through the editorial process by logging in to the press web site:<br />
<br />
Manuscript URL: {$submissionUrl}<br />
Username: {$authorUsername}<br />
<br />
If you have any questions, please contact me. Thank you for considering this press as a venue for your work.<br />
<br />
{$editorialContactSignature}',
  'emails.submissionAck.description' => 'This email, when enabled, is automatically sent to an author when he or she completes the process of submitting a manuscript to the press. It provides information about tracking the submission through the process and thanks the author for the submission.',
  'emails.submissionAckNotUser.subject' => 'Submission Acknowledgement',
  'emails.submissionAckNotUser.body' => 'Hello,<br />
<br />
{$submitterName} has submitted the manuscript, &quot;{$submissionTitle}&quot; to {$contextName}. <br />
<br />
If you have any questions, please contact me. Thank you for considering this press as a venue for your work.<br />
<br />
{$editorialContactSignature}',
  'emails.submissionAckNotUser.description' => 'This email, when enabled, is automatically sent to the other authors who are not users within OMP specified during the submission process.',
  'emails.editorAssign.subject' => 'Editorial Assignment',
  'emails.editorAssign.body' => '{$editorialContactName}:<br />
<br />
The submission, &quot;{$submissionTitle},&quot; to {$contextName} has been assigned to you to see through the editorial process in your role as an Editor.<br />
<br />
Submission URL: {$submissionUrl}<br />
Username: {$editorUsername}<br />
<br />
Thank you,',
  'emails.editorAssign.description' => 'This email notifies a Series Editor that the Editor has assigned them the task of overseeing a submission through the editing process. It provides information about the submission and how to access the press site.',
  'emails.reviewRequest.subject' => 'Manuscript Review Request',
  'emails.reviewRequest.body' => 'Dear {$reviewerName},<br />
<br />
{$messageToReviewer}<br />
<br />
Please log into the press web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />
<br />
The review itself is due {$reviewDueDate}.<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Username: {$reviewerUserName}<br />
<br />
Thank you for considering this request.<br />
<br />
<br />
Sincerely,<br />
{$editorialContactSignature}<br />
',
  'emails.reviewRequest.description' => 'This email from the Series Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST_ATTACHED.)',
  'emails.reviewRequestOneclick.subject' => 'Manuscript Review Request',
  'emails.reviewRequestOneclick.body' => '{$reviewerName}:<br />
<br />
I believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; which has been submitted to {$contextName}. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />
<br />
Please log into the press web site by {$weekLaterDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />
<br />
The review itself is due {$reviewDueDate}.<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Thank you for considering this request.<br />
<br />
{$editorialContactSignature}<br />
<br />
<br />
<br />
&quot;{$submissionTitle}&quot;<br />
<br />
{$abstractTermIfEnabled}<br />
{$submissionAbstract}',
  'emails.reviewRequestOneclick.description' => 'This email from the Series Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review, and one-click reviewer access is enabled.',
  'emails.reviewRequestRemindAuto.subject' => 'Manuscript Review Request',
  'emails.reviewRequestRemindAuto.body' => 'Dear {$reviewerName},<br />
Just a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have your response by {$responseDueDate}, and this email has been automatically generated and sent with the passing of that date.
<br />
{$messageToReviewer}<br />
<br />
Please log into the press web site to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />
<br />
The review itself is due {$reviewDueDate}.<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Username: {$reviewerUserName}<br />
<br />
Thank you for considering this request.<br />
<br />
<br />
Sincerely,<br />
{$editorialContactSignature}<br />
',
  'emails.reviewRequestRemindAuto.description' => 'This email is automatically sent when a reviewer\'s confirmation due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).',
  'emails.reviewRequestRemindAutoOneclick.subject' => 'Manuscript Review Request',
  'emails.reviewRequestRemindAutoOneclick.body' => '{$reviewerName}:<br />
Just a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have your response by {$responseDueDate}, and this email has been automatically generated and sent with the passing of that date.
<br />
I believe that you would serve as an excellent reviewer of the manuscript. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />
<br />
Please log into the press web site to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />
<br />
The review itself is due {$reviewDueDate}.<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Thank you for considering this request.<br />
<br />
{$editorialContactSignature}<br />
<br />
<br />
<br />
&quot;{$submissionTitle}&quot;<br />
<br />
{$abstractTermIfEnabled}<br />
{$submissionAbstract}',
  'emails.reviewRequestRemindAutoOneclick.description' => 'This email is automatically sent when a reviewer\'s confirmation due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is enabled. Scheduled tasks must be enabled and configured (see the site configuration file).',
  'emails.reviewRequestAttached.subject' => 'Manuscript Review Request',
  'emails.reviewRequestAttached.body' => '{$reviewerName}:<br />
<br />
I believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; and I am asking that you consider undertaking this important task for us. The Review Guidelines for this press are appended below, and the submission is attached to this email. Your review of the submission, along with your recommendation, should be emailed to me by {$reviewDueDate}.<br />
<br />
Please indicate in a return email by {$weekLaterDate} whether you are able and willing to do the review.<br />
<br />
Thank you for considering this request.<br />
<br />
{$editorialContactSignature}<br />
<br />
<br />
Review Guidelines<br />
<br />
{$reviewGuidelines}<br />
',
  'emails.reviewRequestAttached.description' => 'This email is sent by the Series Editor to a Reviewer to request that they accept or decline the task of reviewing a submission. It includes the submission as an attachment. This message is used when the Email-Attachment Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST.)',
  'emails.reviewCancel.subject' => 'Request for Review Cancelled',
  'emails.reviewCancel.body' => '{$reviewerName}:<br />
<br />
We have decided at this point to cancel our request for you to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We apologize for any inconvenience this may cause you and hope that we will be able to call on you to assist with this review process in the future.<br />
<br />
If you have any questions, please contact me.',
  'emails.reviewCancel.description' => 'This email is sent by the Series Editor to a Reviewer who has a submission review in progress to notify them that the review has been cancelled.',
  'emails.reviewReinstate.subject' => 'Request for Review Reinstated',
  'emails.reviewReinstate.body' => '{$reviewerName}:<br />
<br />
We would like to reinstate our request for you to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We hope that you will be able to assist with this journal\'s review process.<br />
<br />
If you have any questions, please contact me.',
  'emails.reviewReinstate.description' => 'This email is sent by the Section Editor to a Reviewer who has a submission review in progress to notify them that the review has been cancelled.',
  'emails.reviewConfirm.subject' => 'Able to Review',
  'emails.reviewConfirm.body' => 'Editor(s):<br />
<br />
I am able and willing to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. Thank you for thinking of me, and I plan to have the review completed by its due date, {$reviewDueDate}, if not before.<br />
<br />
{$reviewerName}',
  'emails.reviewConfirm.description' => 'This email is sent by a Reviewer to the Series Editor in response to a review request to notify the Series Editor that the review request has been accepted and will be completed by the specified date.',
  'emails.reviewDecline.subject' => 'Unable to Review',
  'emails.reviewDecline.body' => 'Editor(s):<br />
<br />
I am afraid that at this time I am unable to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. Thank you for thinking of me, and another time feel free to call on me.<br />
<br />
{$reviewerName}',
  'emails.reviewDecline.description' => 'This email is sent by a Reviewer to the Series Editor in response to a review request to notify the Series Editor that the review request has been declined.',
  'emails.reviewAck.subject' => 'Manuscript Review Acknowledgement',
  'emails.reviewAck.body' => '{$reviewerName}:<br />
<br />
Thank you for completing the review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We appreciate your contribution to the quality of the work that we publish.',
  'emails.reviewAck.description' => 'This email is sent by a Series Editor to confirm receipt of a completed review and thank the reviewer for their contributions.',
  'emails.reviewRemind.subject' => 'Submission Review Reminder',
  'emails.reviewRemind.body' => '{$reviewerName}:<br />
<br />
Just a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and would be pleased to receive it as soon as you are able to prepare it.<br />
<br />
If you do not have your username and password for the web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Username: {$reviewerUserName}<br />
<br />
Please confirm your ability to complete this vital contribution to the work of the press. I look forward to hearing from you.<br />
<br />
{$editorialContactSignature}',
  'emails.reviewRemind.description' => 'This email is sent by a Series Editor to remind a reviewer that their review is due.',
  'emails.reviewRemindOneclick.subject' => 'Submission Review Reminder',
  'emails.reviewRemindOneclick.body' => '{$reviewerName}:<br />
<br />
Just a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and would be pleased to receive it as soon as you are able to prepare it.<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Please confirm your ability to complete this vital contribution to the work of the press. I look forward to hearing from you.<br />
<br />
{$editorialContactSignature}',
  'emails.reviewRemindOneclick.description' => 'This email is sent by a Series Editor to remind a reviewer that their review is due.',
  'emails.reviewRemindAuto.subject' => 'Automated Submission Review Reminder',
  'emails.reviewRemindAuto.body' => '{$reviewerName}:<br />
<br />
Just a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and this email has been automatically generated and sent with the passing of that date. We would still be pleased to receive it as soon as you are able to prepare it.<br />
<br />
If you do not have your username and password for the web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Username: {$reviewerUserName}<br />
<br />
Please confirm your ability to complete this vital contribution to the work of the press. I look forward to hearing from you.<br />
<br />
{$editorialContactSignature}',
  'emails.reviewRemindAuto.description' => 'This email is automatically sent when a reviewer\'s due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).',
  'emails.reviewRemindAutoOneclick.subject' => 'Automated Submission Review Reminder',
  'emails.reviewRemindAutoOneclick.body' => '{$reviewerName}:<br />
<br />
Just a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and this email has been automatically generated and sent with the passing of that date. We would still be pleased to receive it as soon as you are able to prepare it.<br />
<br />
Submission URL: {$submissionReviewUrl}<br />
<br />
Please confirm your ability to complete this vital contribution to the work of the press. I look forward to hearing from you.<br />
<br />
{$editorialContactSignature}',
  'emails.reviewRemindAutoOneclick.description' => 'This email is automatically sent when a reviewer\'s due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is enabled. Scheduled tasks must be enabled and configured (see the site configuration file).',
  'emails.editorDecisionAccept.subject' => 'Editor Decision',
  'emails.editorDecisionAccept.body' => '{$authorName}:<br />
<br />
We have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />
<br />
Our decision is to:<br />
<br />
Manuscript URL: {$submissionUrl}',
  'emails.editorDecisionAccept.description' => 'This email from the Editor or Series Editor to an Author notifies them of a final decision regarding their submission.',
  'emails.editorDecisionSendToExternal.subject' => 'Editor Decision',
  'emails.editorDecisionSendToExternal.body' => '{$authorName}:<br />
<br />
We have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />
<br />
Our decision is to:<br />
<br />
Manuscript URL: {$submissionUrl}',
  'emails.editorDecisionSendToExternal.description' => 'This email from the Editor or Series Editor to an Author notifies them that their submission is being sent to an external review.',
  'emails.editorDecisionSendToProduction.subject' => 'Editor Decision',
  'emails.editorDecisionSendToProduction.body' => '{$authorName}:<br />
<br />
The editing of your manuscript, &quot;{$submissionTitle},&quot; is complete.  We are now sending it to production.<br />
<br />
Manuscript URL: {$submissionUrl}',
  'emails.editorDecisionSendToProduction.description' => 'This email from the Editor or Series Editor to an Author notifies them that their submission is being sent to production.',
  'emails.editorDecisionRevisions.subject' => 'Editor Decision',
  'emails.editorDecisionRevisions.body' => '{$authorName}:<br />
<br />
We have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />
<br />
Our decision is to:<br />
<br />
Manuscript URL: {$submissionUrl}',
  'emails.editorDecisionRevisions.description' => 'This email from the Editor or Series Editor to an Author notifies them of a final decision regarding their submission.',
  'emails.editorDecisionResubmit.subject' => 'Editor Decision',
  'emails.editorDecisionResubmit.body' => '{$authorName}:<br />
<br />
We have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />
<br />
Our decision is to:<br />
<br />
Manuscript URL: {$submissionUrl}',
  'emails.editorDecisionResubmit.description' => 'This email from the Editor or Series Editor to an Author notifies them of a final decision regarding their submission.',
  'emails.editorDecisionDecline.subject' => 'Editor Decision',
  'emails.editorDecisionDecline.body' => '{$authorName}:<br />
<br />
We have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />
<br />
Our decision is to:<br />
<br />
Manuscript URL: {$submissionUrl}',
  'emails.editorDecisionDecline.description' => 'This email from the Editor or Series Editor to an Author notifies them of a final decision regarding their submission.',
  'emails.editorRecommendation.subject' => 'Editor Recommendation',
  'emails.editorRecommendation.body' => '{$editors}:<br />
<br />
The recommendation regarding the submission to {$contextName}, &quot;{$submissionTitle}&quot; is: {$recommendation}',
  'emails.editorRecommendation.description' => 'This email from the recommending Editor or Section Editor to the decision making Editors or Section Editors notifies them of a final recommendation regarding the submission.',
  'emails.copyeditRequest.subject' => 'Copyediting Request',
  'emails.copyeditRequest.body' => '{$participantName}:<br />
<br />
I would ask that you undertake the copyediting of &quot;{$submissionTitle}&quot; for {$contextName} by following these steps.<br />
1. Click on the Submission URL below.<br />
2. Log into the press and click on the File that appears in Step 1.<br />
3. Consult Copyediting Instructions posted on webpage.<br />
4. Open the downloaded file and copyedit, while adding Author Queries as needed.<br />
5. Save copyedited file, and upload to Step 1 of Copyediting.<br />
6. Send the COMPLETE email to the editor.<br />
<br />
{$contextName} URL: {$contextUrl}<br />
Submission URL: {$submissionUrl}<br />
Username: {$participantUsername}',
  'emails.copyeditRequest.description' => 'This email is sent by a Series Editor to a submission\'s Copyeditor to request that they begin the copyediting process. It provides information about the submission and how to access it.',
  'emails.layoutRequest.subject' => 'Request Galleys',
  'emails.layoutRequest.body' => '{$participantName}:<br />
<br />
The submission &quot;{$submissionTitle}&quot; to {$contextName} now needs galleys laid out by following these steps.<br />
1. Click on the Submission URL below.<br />
2. Log into the press and use the Layout Version file to create the galleys according to press standards.<br />
3. Send the COMPLETE email to the editor.<br />
<br />
{$contextName} URL: {$contextUrl}<br />
Submission URL: {$submissionUrl}<br />
Username: {$participantUsername}<br />
<br />
If you are unable to undertake this work at this time or have any questions, please contact me. Thank you for your contribution to this press.',
  'emails.layoutRequest.description' => 'This email from the Series Editor to the Layout Editor notifies them that they have been assigned the task of performing layout editing on a submission. It provides information about the submission and how to access it.',
  'emails.layoutComplete.subject' => 'Layout Galleys Complete',
  'emails.layoutComplete.body' => '{$editorialContactName}:<br />
<br />
Galleys have now been prepared for the manuscript, &quot;{$submissionTitle},&quot; for {$contextName} and are ready for proofreading.<br />
<br />
If you have any questions, please contact me.<br />
<br />
{$signatureFullName}',
  'emails.layoutComplete.description' => 'This email from the Layout Editor to the Series Editor notifies them that the layout process has been completed.',
  'emails.indexRequest.subject' => 'Request Index',
  'emails.indexRequest.body' => '{$participantName}:<br />
<br />
The submission &quot;{$submissionTitle}&quot; to {$contextName} now needs indexes created by following these steps.<br />
1. Click on the Submission URL below.<br />
2. Log into the press and use the Page Proofs file to create the galleys according to press standards.<br />
3. Send the COMPLETE email to the editor.<br />
<br />
{$contextName} URL: {$contextUrl}<br />
Submission URL: {$submissionUrl}<br />
Username: {$participantUsername}<br />
<br />
If you are unable to undertake this work at this time or have any questions, please contact me. Thank you for your contribution to this press.<br />
<br />
{$editorialContactSignature}',
  'emails.indexRequest.description' => 'This email from the Series Editor to the Indexer notifies them that they have been assigned the task of creating indexes for a submission. It provides information about the submission and how to access it.',
  'emails.indexComplete.subject' => 'Index Galleys Complete',
  'emails.indexComplete.body' => '{$editorialContactName}:<br />
<br />
Indexes have now been prepared for the manuscript, &quot;{$submissionTitle},&quot; for {$contextName} and are ready for proofreading.<br />
<br />
If you have any questions, please contact me.<br />
<br />
{$signatureFullName}',
  'emails.indexComplete.description' => 'This email from the Indexer to the Series Editor notifies them that the index creation process has been completed.',
  'emails.emailLink.subject' => 'Manuscript of Possible Interest',
  'emails.emailLink.body' => 'Thought you might be interested in seeing &quot;{$submissionTitle}&quot; by {$authorName} published in Vol {$volume}, No {$number} ({$year}) of {$contextName} at &quot;{$monographUrl}&quot;.',
  'emails.emailLink.description' => 'This email template provides a registered reader with the opportunity to send information about a monograph to somebody who may be interested. It is available via the Reading Tools and must be enabled by the Press Manager in the Reading Tools Administration page.',
  'emails.notifySubmission.subject' => 'Submission Notification',
  'emails.notifySubmission.body' => 'You have a message from {$sender} regarding &quot;{$submissionTitle}&quot; ({$monographDetailsUrl}):<br />
<br />
		{$message}<br />
<br />
		',
  'emails.notifySubmission.description' => 'A notification from a user sent from a submission information center modal.',
  'emails.notifyFile.subject' => 'Submission File Notification',
  'emails.notifyFile.body' => 'You have a message from {$sender} regarding the file &quot;{$fileName}&quot; in &quot;{$submissionTitle}&quot; ({$monographDetailsUrl}):<br />
<br />
		{$message}<br />
<br />
		',
  'emails.notifyFile.description' => 'A notification from a user sent from a file information center modal',
  'emails.notificationCenterDefault.subject' => 'A message regarding {$contextName}',
  'emails.notificationCenterDefault.body' => 'Please enter your message.',
  'emails.notificationCenterDefault.description' => 'The default (blank) message used in the Notification Center Message Listbuilder.',
  'emails.editorDecisionInitialDecline.subject' => 'Editor Decision',
  'emails.editorDecisionInitialDecline.body' => '
			{$authorName}:<br />
<br />
We have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />
<br />
Our decision is to: Decline Submission<br />
<br />
Manuscript URL: {$submissionUrl}
		',
  'emails.editorDecisionInitialDecline.description' => 'This email is send to the author if the editor declines his submission initially, before the review stage',
  'emails.statisticsReportNotification.subject' => 'Editorial activity for {$month}, {$year}',
  'emails.statisticsReportNotification.body' => '
{$name}, <br />
<br />
Your press health report for {$month}, {$year} is now available. Your key stats for this month are below.<br />
<ul>
	<li>New submissions this month: {$newSubmissions}</li>
	<li>Declined submissions this month: {$declinedSubmissions}</li>
	<li>Accepted submissions this month: {$acceptedSubmissions}</li>
	<li>Total submissions in the system: {$totalSubmissions}</li>
</ul>
Login to the the press to view more detailed <a href="{$editorialStatsLink}">editorial trends</a> and <a href="{$publicationStatsLink}">published article stats</a>. A full copy of this month\'s editorial trends is attached.<br />
<br />
Sincerely,<br />
{$principalContactSignature}',
  'emails.statisticsReportNotification.description' => 'This email is automatically sent monthly to editors and journal managers to provide them a system health overview.',
  'emails.announcement.subject' => '{$title}',
  'emails.announcement.body' => '<b>{$title}</b><br />
<br />
{$summary}<br />
<br />
Visit our website to read the <a href="{$url}">full announcement</a>.',
  'emails.announcement.description' => 'This email is sent when a new announcement is created.',
);