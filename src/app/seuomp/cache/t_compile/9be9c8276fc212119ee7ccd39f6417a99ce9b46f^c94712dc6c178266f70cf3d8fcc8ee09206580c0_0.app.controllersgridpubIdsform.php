<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:13:11
  from 'app:controllersgridpubIdsform' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a1871c4598_13647151',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c94712dc6c178266f70cf3d8fcc8ee09206580c0' => 
    array (
      0 => 'app:controllersgridpubIdsform',
      1 => 1647574007,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6234a1871c4598_13647151 (Smarty_Internal_Template $_smarty_tpl) {
?> <?php echo '<script'; ?>
>
	$(function() {
		// Attach the form handler.
		$('#assignPublicIdentifierForm').pkpHandler(
			'$.pkp.controllers.form.AjaxFormHandler',
			{
				trackFormChanges: true
			}
		);
	});
<?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['pubObject']->value instanceof Representation) {?>
	<form class="pkp_form" id="assignPublicIdentifierForm" method="post" action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('component'=>"grid.catalogEntry.PublicationFormatGridHandler",'op'=>"setApproved",'submissionId'=>$_smarty_tpl->tpl_vars['submissionId']->value,'publicationId'=>$_smarty_tpl->tpl_vars['pubObject']->value->getData('publicationId'),'representationId'=>$_smarty_tpl->tpl_vars['pubObject']->value->getId(),'newApprovedState'=>$_smarty_tpl->tpl_vars['approval']->value,'confirmed'=>true,'escape'=>false),$_smarty_tpl ) );?>
">
		<?php $_smarty_tpl->_assignInScope('remoteObject', $_smarty_tpl->tpl_vars['pubObject']->value->getRemoteURL());
} elseif ($_smarty_tpl->tpl_vars['pubObject']->value instanceof SubmissionFile) {?>
	<form class="pkp_form" id="assignPublicIdentifierForm" method="post" action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('component'=>"grid.catalogEntry.PublicationFormatGridHandler",'op'=>"setProofFileCompletion",'submissionFileId'=>$_smarty_tpl->tpl_vars['pubObject']->value->getId(),'submissionId'=>$_smarty_tpl->tpl_vars['pubObject']->value->getData('submissionId'),'publicationId'=>$_smarty_tpl->tpl_vars['publicationId']->value,'approval'=>$_smarty_tpl->tpl_vars['approval']->value,'confirmed'=>true,'escape'=>false),$_smarty_tpl ) );?>
">
<?php }
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['csrf'][0], array( array(),$_smarty_tpl ) );?>

<?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['fbvFormArea'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['fbvFormArea'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'smartyFBVFormArea'))) {
throw new SmartyException('block tag \'fbvFormArea\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('fbvFormArea', array('id'=>"confirmationText"));
$_block_repeat=true;
echo $_block_plugin1->smartyFBVFormArea(array('id'=>"confirmationText"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
	<p><?php echo $_smarty_tpl->tpl_vars['confirmationText']->value;?>
</p>
<?php $_block_repeat=false;
echo $_block_plugin1->smartyFBVFormArea(array('id'=>"confirmationText"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
if ($_smarty_tpl->tpl_vars['approval']->value) {?>
	<?php if (!$_smarty_tpl->tpl_vars['remoteObject']->value) {?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubIdPlugins']->value, 'pubIdPlugin');
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pubIdPlugin']->value) {
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = false;
?>
			<?php $_smarty_tpl->_assignInScope('pubIdAssignFile', $_smarty_tpl->tpl_vars['pubIdPlugin']->value->getPubIdAssignFile());?>
			<?php $_smarty_tpl->_assignInScope('canBeAssigned', $_smarty_tpl->tpl_vars['pubIdPlugin']->value->canBeAssigned($_smarty_tpl->tpl_vars['pubObject']->value));?>
			<?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['pubIdAssignFile']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pubIdPlugin'=>$_smarty_tpl->tpl_vars['pubIdPlugin']->value,'pubObject'=>$_smarty_tpl->tpl_vars['pubObject']->value,'canBeAssigned'=>$_smarty_tpl->tpl_vars['canBeAssigned']->value), 0, true);
?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	<?php }
}
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['fbvFormButtons'][0], array( array('id'=>"assignPublicIdentifierForm",'submitText'=>"common.ok"),$_smarty_tpl ) );?>

</form>
<?php }
}
