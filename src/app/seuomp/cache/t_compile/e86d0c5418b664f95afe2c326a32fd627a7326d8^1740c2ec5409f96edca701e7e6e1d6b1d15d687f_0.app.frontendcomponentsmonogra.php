<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:32:49
  from 'app:frontendcomponentsmonogra' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a621024522_47246536',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1740c2ec5409f96edca701e7e6e1d6b1d15d687f' => 
    array (
      0 => 'app:frontendcomponentsmonogra',
      1 => 1647574020,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/objects/monograph_summary.tpl' => 1,
  ),
),false)) {
function content_6234a621024522_47246536 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section>
<div class="row gx-5">


	

   
       <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['monographs']->value, 'monograph', false, NULL, 'monographListLoop', array (
));
$_smarty_tpl->tpl_vars['monograph']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['monograph']->value) {
$_smarty_tpl->tpl_vars['monograph']->do_else = false;
?>   
                  <?php $_smarty_tpl->_subTemplateRender("app:frontend/objects/monograph_summary.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('monograph'=>$_smarty_tpl->tpl_vars['monograph']->value,'isFeatured'=>$_smarty_tpl->tpl_vars['isFeatured']->value,'heading'=>$_smarty_tpl->tpl_vars['monographHeading']->value), 0, true);
?>  
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

   

    
    

	 


    
    </div>
    <!-- News block -->
  </section><?php }
}
