<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:14:02
  from 'app:frontendcomponentspublica' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a1ba0457a3_14021471',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '99c9be253591891f5a79d494bb42ab25c4450428' => 
    array (
      0 => 'app:frontendcomponentspublica',
      1 => 1647574020,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/downloadLink.tpl' => 2,
  ),
),false)) {
function content_6234a1ba0457a3_14021471 (Smarty_Internal_Template $_smarty_tpl) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['publicationFormats']->value, 'format');
$_smarty_tpl->tpl_vars['format']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['format']->value) {
$_smarty_tpl->tpl_vars['format']->do_else = false;
?>
	<?php $_smarty_tpl->_assignInScope('publicationFormatId', $_smarty_tpl->tpl_vars['format']->value->getId());?>

		<?php if ($_smarty_tpl->tpl_vars['format']->value->getRemoteUrl() && !$_smarty_tpl->tpl_vars['isChapterRequest']->value) {?>
				<div class="pub_format_<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationFormatId']->value ));?>
 pub_format_remote">
			<button class="btn btn-warning" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['format']->value->getRemoteURL() ));?>
" target="_blank" class="remote_resource">
				<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['format']->value->getLocalizedName() ));?>

			</button>
		</div>

		<?php } else { ?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pluck_files'][0], array( array('assign'=>'pubFormatFiles','files'=>$_smarty_tpl->tpl_vars['publicationFiles']->value,'by'=>"publicationFormat",'value'=>$_smarty_tpl->tpl_vars['format']->value->getId()),$_smarty_tpl ) );?>


				<?php if (count($_smarty_tpl->tpl_vars['pubFormatFiles']->value) == 1) {?>
			<div class="pub_format_<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationFormatId']->value ));?>
 pub_format_single">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubFormatFiles']->value, 'file');
$_smarty_tpl->tpl_vars['file']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->do_else = false;
?>
					<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/downloadLink.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('downloadFile'=>$_smarty_tpl->tpl_vars['file']->value,'monograph'=>$_smarty_tpl->tpl_vars['monograph']->value,'publication'=>$_smarty_tpl->tpl_vars['publication']->value,'publicationFormat'=>$_smarty_tpl->tpl_vars['format']->value,'currency'=>$_smarty_tpl->tpl_vars['currency']->value), 0, true);
?>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</div>

					<?php } elseif (count($_smarty_tpl->tpl_vars['pubFormatFiles']->value) > 1) {?>
			<div class="pub_format_<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationFormatId']->value ));?>
">
				<span class="label">
					<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['format']->value->getLocalizedName() ));?>

				</span>
				<span class="value">
					<ul>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubFormatFiles']->value, 'file');
$_smarty_tpl->tpl_vars['file']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->do_else = false;
?>
							<li>
								<span class="name">
									<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value->getLocalizedData('name') ));?>

								</span>
								<span class="link">
									<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/downloadLink.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('downloadFile'=>$_smarty_tpl->tpl_vars['file']->value,'monograph'=>$_smarty_tpl->tpl_vars['monograph']->value,'publication'=>$_smarty_tpl->tpl_vars['publication']->value,'publicationFormat'=>$_smarty_tpl->tpl_vars['format']->value,'currency'=>$_smarty_tpl->tpl_vars['currency']->value,'useFilename'=>true), 0, true);
?>
								</span>
							</li>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</ul>
				</span><!-- .value -->
			</div>
		<?php }?>
	<?php }
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
