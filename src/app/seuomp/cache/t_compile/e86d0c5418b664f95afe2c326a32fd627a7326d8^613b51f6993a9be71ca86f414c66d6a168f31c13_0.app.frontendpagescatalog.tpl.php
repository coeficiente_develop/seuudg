<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:32:48
  from 'app:frontendpagescatalog.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a620f2a4c0_25894717',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '613b51f6993a9be71ca86f414c66d6a168f31c13' => 
    array (
      0 => 'app:frontendpagescatalog.tpl',
      1 => 1647574021,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/header.tpl' => 1,
    'app:frontend/components/monographList.tpl' => 1,
    'app:frontend/components/footer.tpl' => 1,
  ),
),false)) {
function content_6234a620f2a4c0_25894717 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("app:frontend/components/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pageTitle'=>"navigation.catalog"), 0, false);
?>


<div class="card">
  <div class="card-header">
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"navigation.catalog"),$_smarty_tpl ) );?>

  </div>
  <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"catalog.browseTitles",'numTitles'=>$_smarty_tpl->tpl_vars['total']->value),$_smarty_tpl ) );?>
</p>
          </blockquote>
  </div>
</div>	



<main class="my-5">
    
   

    <div class="container">

    

	<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/monographList.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('monographs'=>$_smarty_tpl->tpl_vars['publishedSubmissions']->value,'featured'=>$_smarty_tpl->tpl_vars['featuredMonographIds']->value), 0, false);
?>
     
    </div>
  </main>


<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
