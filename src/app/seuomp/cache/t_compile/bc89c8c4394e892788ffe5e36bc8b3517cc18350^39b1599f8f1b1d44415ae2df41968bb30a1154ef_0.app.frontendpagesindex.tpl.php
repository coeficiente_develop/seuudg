<?php
/* Smarty version 3.1.39, created on 2022-03-18 13:47:31
  from 'app:frontendpagesindex.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_62348d73741123_55712042',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '39b1599f8f1b1d44415ae2df41968bb30a1154ef' => 
    array (
      0 => 'app:frontendpagesindex.tpl',
      1 => 1647574021,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/header.tpl' => 1,
    'app:frontend/components/footer.tpl' => 1,
  ),
),false)) {
function content_62348d73741123_55712042 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("app:frontend/components/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Masthead-->
        <header class="masthead">
            <div class="container position-relative">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <div class="text-center text-white">
                            <!-- Page heading-->
                            <h1 class="mb-5">Sistema Editorial Universitario</h1>
                                                        <!-- Signup form-->
                            <!-- * * * * * * * * * * * * * * *-->
                            <!-- * * SB Forms Contact Form * *-->
                            <!-- * * * * * * * * * * * * * * *-->
                            <!-- This form is pre-integrated with SB Forms.-->
                            <!-- To make this form functional, sign up at-->
                            <!-- https://startbootstrap.com/solution/contact-forms-->
                            <!-- to get an API token!-->
                            
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Icons Grid-->
        
        <section class="features-icons bg-light text-center">
            <h2>Publica con UDG</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-window m-auto text-primary"></i></div>
                            
                            <h3>
                            <a class="text-dark" href="">¿Como publicar en UDG?</a>
                            </h3>
                            <p class="lead mb-0">Ven y publica con nosotros</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-file-earmark-check m-auto text-primary"></i></div>
                            <h3>
                            <a class="text-dark" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"about",'op'=>"submissions"),$_smarty_tpl ) );?>
">Requisitos y Normatividad</a>
                            
                            </h3>
                            <p class="lead mb-0">Por favor lea los requisitos antes de solicitar</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-file-text m-auto text-primary"></i></div>
                            <h3>
                            <a class="text-dark" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"about",'op'=>"privacy"),$_smarty_tpl ) );?>
">Aviso de Privacidad</a>
                            </h3>
                            <p class="lead mb-0">Por favor consulte el aviso de privacidad</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-chat-right-text-fill m-auto text-primary"></i></div>
                            <h3>
                            <a class="text-dark" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"about",'op'=>"contact"),$_smarty_tpl ) );?>
">Contactanos</a>
                            </h3>
                            <p class="lead mb-0">Ponte en contacto con nosotros</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="features-icons bg-light text-center">
            <h2>Sitios de Interes</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-gear-wide-connected m-auto text-primary"></i></div>
                            <h6>COORDINACION GENERAL DE INVESTIGACION,POSGRADO Y VINCULACION</h6>
                            <!-- <p class="lead mb-0">Ven y publica con nosotros</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-bank m-auto text-primary"></i></div>
                            <h5>
                            <a class="text-dark" href="https://www.udg.mx/">UNIVERSIDAD DE GUADALAJARA</a>
                            
                            </h5>
                            <!-- <p class="lead mb-0">Por favor lea los requisitos antes de solicitar</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-journals m-auto text-primary"></i></div>
                            <h5>
                            <a class="text-dark" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"catalog"),$_smarty_tpl ) );?>
">
                            Catalogo
                            </a>
                            </h5>
                            <!-- <p class="lead mb-0">Por favor consulte el reglamento editorial</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>


<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
