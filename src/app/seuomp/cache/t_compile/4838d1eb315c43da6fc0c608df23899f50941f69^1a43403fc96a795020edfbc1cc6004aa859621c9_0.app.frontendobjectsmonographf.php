<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:14:02
  from 'app:frontendobjectsmonographf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a1ba00dd59_05956964',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1a43403fc96a795020edfbc1cc6004aa859621c9' => 
    array (
      0 => 'app:frontendobjectsmonographf',
      1 => 1647574021,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/publicationFormats.tpl' => 1,
    'app:frontend/components/downloadLink.tpl' => 1,
  ),
),false)) {
function content_6234a1ba00dd59_05956964 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/app/seuomp/lib/pkp/lib/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<div class="container pt-3">
	<div class="row g-2">

        <div class="">
			<div class="bg-light text-center p-2">
				<?php $_smarty_tpl->_assignInScope('coverImage', $_smarty_tpl->tpl_vars['publication']->value->getLocalizedData('coverImage'));?>
				<img src="<?php echo $_smarty_tpl->tpl_vars['publication']->value->getLocalizedCoverImageThumbnailUrl($_smarty_tpl->tpl_vars['monograph']->value->getData('contextId'));?>
"
					alt="<?php echo (($tmp = @call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['coverImage']->value['altText'] )))===null||$tmp==='' ? '' : $tmp);?>
">
			</div>
		</div>
        
        <div class="">
			<div class="bg-light text-center p-2">
				<h4>Sinopsis/Resumen</h4>
				<p><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'strip_unsafe_html' ][ 0 ], array( $_smarty_tpl->tpl_vars['publication']->value->getLocalizedData('abstract') ));?>
</p>
			</div>
		</div>

        <div class="">
			<div class="bg-light text-center p-2">
				<h4><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publication']->value->getLocalizedFullTitle() ));?>
</h4>
				<?php if ($_smarty_tpl->tpl_vars['currentPublication']->value->getID() !== $_smarty_tpl->tpl_vars['publication']->value->getId()) {?>
				<div class="cmp_notification notice">
					<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "latestVersionUrl", null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"catalog",'op'=>"book",'path'=>$_smarty_tpl->tpl_vars['monograph']->value->getBestId()),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.outdatedVersion",'datePublished'=>smarty_modifier_date_format($_smarty_tpl->tpl_vars['publication']->value->getData('datePublished'),$_smarty_tpl->tpl_vars['dateFormatShort']->value),'urlRecentVersion'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['latestVersionUrl']->value ))),$_smarty_tpl ) );?>

				</div>
				<?php }?>
			</div>
		</div>

		
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pluck_files'][0], array( array('assign'=>'bookFiles','files'=>$_smarty_tpl->tpl_vars['availableFiles']->value,'by'=>"chapter",'value'=>0),$_smarty_tpl ) );?>

		<?php if (count($_smarty_tpl->tpl_vars['bookFiles']->value) || count($_smarty_tpl->tpl_vars['remotePublicationFormats']->value)) {?>
		<div class="">
			<div class="bg-light text-center p-2">
				<h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.downloads"),$_smarty_tpl ) );?>
</h2>
				<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/publicationFormats.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('publicationFiles'=>$_smarty_tpl->tpl_vars['bookFiles']->value), 0, false);
?>
			</div>
		</div>
		<?php }?>

        
		
		

		
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubIdPlugins']->value, 'pubIdPlugin');
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pubIdPlugin']->value) {
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['pubIdPlugin']->value->getPubIdType() != 'doi') {?>
		<?php continue 1;?>
		<?php }?>
		<?php $_smarty_tpl->_assignInScope('pubId', $_smarty_tpl->tpl_vars['monograph']->value->getStoredPubId($_smarty_tpl->tpl_vars['pubIdPlugin']->value->getPubIdType()));?>
		<?php if ($_smarty_tpl->tpl_vars['pubId']->value) {?>
		<?php $_smarty_tpl->_assignInScope('doiUrl', call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pubIdPlugin']->value->getResolvingURL($_smarty_tpl->tpl_vars['currentPress']->value->getId(),$_smarty_tpl->tpl_vars['pubId']->value) )));?>
		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>DOI</h4>
				<a href="<?php echo $_smarty_tpl->tpl_vars['doiUrl']->value;?>
">
					<?php echo $_smarty_tpl->tpl_vars['doiUrl']->value;?>

				</a>
			</div>
		</div>
		<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


		<?php if (!empty($_smarty_tpl->tpl_vars['publication']->value->getLocalizedData('keywords'))) {?>
		<div class="col-12 col-md-6 order-3">
			<div class="bg-light text-center p-2">
				<h4>Keywords</h4>
				<p><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['publication']->value->getLocalizedData('keywords'), 'keyword', false, NULL, 'keywords', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
$_smarty_tpl->tpl_vars['keyword']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['keyword']->value) {
$_smarty_tpl->tpl_vars['keyword']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_keywords']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_keywords']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_keywords']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_keywords']->value['total'];
?>
					<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['keyword']->value ));
if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_keywords']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_keywords']->value['last'] : null)) {?>, <?php }?>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></p>
			</div>
		</div>
		<?php }?>



		


		<?php if (count($_smarty_tpl->tpl_vars['chapters']->value)) {?>
		<div class="col-12 col-md-6 order-3">
			<div class="bg-light text-center p-2">
				<h4>Capitulos</h4>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['chapters']->value, 'chapter');
$_smarty_tpl->tpl_vars['chapter']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['chapter']->value) {
$_smarty_tpl->tpl_vars['chapter']->do_else = false;
?>
				<?php $_smarty_tpl->_assignInScope('chapterId', $_smarty_tpl->tpl_vars['chapter']->value->getId());?>
				<li>
					<?php if ($_smarty_tpl->tpl_vars['chapter']->value->isPageEnabled()) {?>
					<?php if ($_smarty_tpl->tpl_vars['publication']->value->getId() === $_smarty_tpl->tpl_vars['currentPublication']->value->getId()) {?>
					<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>" catalog",'op'=>"book",'path'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'to_array' ][ 0 ], array( $_smarty_tpl->tpl_vars['monograph']->value->getBestId(),"chapter",$_smarty_tpl->tpl_vars['chapter']->value->getSourceChapterId() ))),$_smarty_tpl ) );?>
">
						<?php } else { ?>
						<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>" catalog",'op'=>"book",'path'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'to_array' ][ 0 ], array( $_smarty_tpl->tpl_vars['monograph']->value->getBestId(),"version",$_smarty_tpl->tpl_vars['publication']->value->getId(),"chapter",$_smarty_tpl->tpl_vars['chapter']->value->getSourceChapterId() ))),$_smarty_tpl ) );?>
">
							<?php }?>
							<?php }?>
							<div class="title">
								<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['chapter']->value->getLocalizedTitle() ));?>

								<?php if ($_smarty_tpl->tpl_vars['chapter']->value->getLocalizedSubtitle() != '') {?>
								<div class="subtitle">
									<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['chapter']->value->getLocalizedSubtitle() ));?>

								</div>
								<?php }?>
							</div>
							<?php if ($_smarty_tpl->tpl_vars['chapter']->value->isPageEnabled()) {?>
						</a>
						<?php }?>
						<?php $_smarty_tpl->_assignInScope('chapterAuthors', $_smarty_tpl->tpl_vars['chapter']->value->getAuthorNamesAsString());?>
						<?php if ($_smarty_tpl->tpl_vars['authorString']->value != $_smarty_tpl->tpl_vars['chapterAuthors']->value) {?>
						<div class="authors">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['chapterAuthors']->value ));?>

						</div>
						<?php }?>

												<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubIdPlugins']->value, 'pubIdPlugin');
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pubIdPlugin']->value) {
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = false;
?>
						<?php if ($_smarty_tpl->tpl_vars['pubIdPlugin']->value->getPubIdType() != 'doi') {?>
						<?php continue 1;?>
						<?php }?>
						<?php $_smarty_tpl->_assignInScope('pubId', $_smarty_tpl->tpl_vars['chapter']->value->getStoredPubId($_smarty_tpl->tpl_vars['pubIdPlugin']->value->getPubIdType()));?>
						<?php if ($_smarty_tpl->tpl_vars['pubId']->value) {?>
						<?php $_smarty_tpl->_assignInScope('doiUrl', call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pubIdPlugin']->value->getResolvingURL($_smarty_tpl->tpl_vars['currentPress']->value->getId(),$_smarty_tpl->tpl_vars['pubId']->value) )));?>
						<div class="doi"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.pubIds.doi.readerDisplayName"),$_smarty_tpl ) );?>
 <a
								href="<?php echo $_smarty_tpl->tpl_vars['doiUrl']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['doiUrl']->value;?>
</a></div>
						<?php }?>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

												<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pluck_files'][0], array( array('assign'=>"chapterFiles",'files'=>$_smarty_tpl->tpl_vars['availableFiles']->value,'by'=>"chapter",'value'=>$_smarty_tpl->tpl_vars['chapterId']->value),$_smarty_tpl ) );?>

						<?php if (count($_smarty_tpl->tpl_vars['chapterFiles']->value)) {?>
						<div class="files">

														<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['publicationFormats']->value, 'format');
$_smarty_tpl->tpl_vars['format']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['format']->value) {
$_smarty_tpl->tpl_vars['format']->do_else = false;
?>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pluck_files'][0], array( array('assign'=>"pubFormatFiles",'files'=>$_smarty_tpl->tpl_vars['chapterFiles']->value,'by'=>"publicationFormat",'value'=>$_smarty_tpl->tpl_vars['format']->value->getId()),$_smarty_tpl ) );?>


							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubFormatFiles']->value, 'file');
$_smarty_tpl->tpl_vars['file']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->do_else = false;
?>

														<?php $_smarty_tpl->_assignInScope('useFileName', false);?>
							<?php if (count($_smarty_tpl->tpl_vars['pubFormatFiles']->value) > 1) {?>
							<?php $_smarty_tpl->_assignInScope('useFileName', true);?>
							<?php }?>

							<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/downloadLink.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('downloadFile'=>$_smarty_tpl->tpl_vars['file']->value,'monograph'=>$_smarty_tpl->tpl_vars['monograph']->value,'publicationFormat'=>$_smarty_tpl->tpl_vars['format']->value,'currency'=>$_smarty_tpl->tpl_vars['currency']->value,'useFilename'=>$_smarty_tpl->tpl_vars['useFileName']->value), 0, true);
?>
							<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
							<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						</div>
						<?php }?>
				</li>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</div>
		</div>
		<?php }?>



		<div class="col-12 col-md-6 order-4">
			<div class="bg-light text-center p-2">
				<h4>Biografias</h4>
				<?php $_smarty_tpl->_assignInScope('hasBiographies', 0);?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['publication']->value->getData('authors'), 'author');
$_smarty_tpl->tpl_vars['author']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['author']->value) {
$_smarty_tpl->tpl_vars['author']->do_else = false;
?>
				<?php if ($_smarty_tpl->tpl_vars['author']->value->getLocalizedBiography()) {?>
				<?php $_smarty_tpl->_assignInScope('hasBiographies', $_smarty_tpl->tpl_vars['hasBiographies']->value+1);?>
				<?php }?>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				<?php if ($_smarty_tpl->tpl_vars['hasBiographies']->value) {?>
				<div class="item author_bios">
					<h2 class="label">
						<?php if ($_smarty_tpl->tpl_vars['hasBiographies']->value > 1) {?>
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.authorBiographies"),$_smarty_tpl ) );?>

						<?php } else { ?>
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.authorBiography"),$_smarty_tpl ) );?>

						<?php }?>
					</h2>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['publication']->value->getData('authors'), 'author');
$_smarty_tpl->tpl_vars['author']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['author']->value) {
$_smarty_tpl->tpl_vars['author']->do_else = false;
?>
					<?php if ($_smarty_tpl->tpl_vars['author']->value->getLocalizedBiography()) {?>
					<div class="sub_item">
						<div class="label">
							<?php if ($_smarty_tpl->tpl_vars['author']->value->getLocalizedAffiliation()) {?>
							<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "authorName", null);
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['author']->value->getFullName() ));
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
							<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "authorAffiliation", null);?><span
								class="affiliation"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['author']->value->getLocalizedAffiliation() ));?>
</span><?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.authorWithAffiliation",'name'=>$_smarty_tpl->tpl_vars['authorName']->value,'affiliation'=>$_smarty_tpl->tpl_vars['authorAffiliation']->value),$_smarty_tpl ) );?>

							<?php } else { ?>
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['author']->value->getFullName() ));?>

							<?php }?>
						</div>
						<div class="value">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'strip_unsafe_html' ][ 0 ], array( $_smarty_tpl->tpl_vars['author']->value->getLocalizedBiography() ));?>

						</div>
					</div>
					<?php }?>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</div>
				<?php }?>
			</div>
		</div>

		<div class="col-12 col-md-6 order-4">
			<div class="bg-light text-center p-2">
				<h4>Referencias</h4>

				<?php if ($_smarty_tpl->tpl_vars['citations']->value || $_smarty_tpl->tpl_vars['publication']->value->getData('citationsRaw')) {?>
				<div class="item references">
					<h2 class="label">
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.citations"),$_smarty_tpl ) );?>

					</h2>
					<div class="value">
						<?php if ($_smarty_tpl->tpl_vars['citations']->value) {?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['citations']->value, 'citation');
$_smarty_tpl->tpl_vars['citation']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['citation']->value) {
$_smarty_tpl->tpl_vars['citation']->do_else = false;
?>
						<p><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'strip_unsafe_html' ][ 0 ], array( $_smarty_tpl->tpl_vars['citation']->value->getCitationWithLinks() ));?>
</p>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						<?php } else { ?>
						<?php echo nl2br(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publication']->value->getData('citationsRaw') )));?>

						<?php }?>
					</div>
				</div>
				<?php }?>
			</div>
		</div>








		<div class="col-12 col-md-6 order-5">
			<div class="bg-light text-center p-2">
				<h4>Formatos de Publicacion</h4>
								<?php if (count($_smarty_tpl->tpl_vars['publicationFormats']->value)) {?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['publicationFormats']->value, 'publicationFormat');
$_smarty_tpl->tpl_vars['publicationFormat']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['publicationFormat']->value) {
$_smarty_tpl->tpl_vars['publicationFormat']->do_else = false;
?>
				<?php if ($_smarty_tpl->tpl_vars['publicationFormat']->value->getIsApproved()) {?>

				<?php $_smarty_tpl->_assignInScope('identificationCodes', $_smarty_tpl->tpl_vars['publicationFormat']->value->getIdentificationCodes());?>
				<?php $_smarty_tpl->_assignInScope('identificationCodes', $_smarty_tpl->tpl_vars['identificationCodes']->value->toArray());?>
				<?php $_smarty_tpl->_assignInScope('publicationDates', $_smarty_tpl->tpl_vars['publicationFormat']->value->getPublicationDates());?>
				<?php $_smarty_tpl->_assignInScope('publicationDates', $_smarty_tpl->tpl_vars['publicationDates']->value->toArray());?>
				<?php $_smarty_tpl->_assignInScope('hasPubId', false);?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubIdPlugins']->value, 'pubIdPlugin');
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pubIdPlugin']->value) {
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = false;
?>
				<?php $_smarty_tpl->_assignInScope('pubIdType', $_smarty_tpl->tpl_vars['pubIdPlugin']->value->getPubIdType());?>
				<?php if ($_smarty_tpl->tpl_vars['publicationFormat']->value->getStoredPubId($_smarty_tpl->tpl_vars['pubIdType']->value)) {?>
				<?php $_smarty_tpl->_assignInScope('hasPubId', true);?>
				<?php break 1;?>
				<?php }?>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

								<?php if (!$_smarty_tpl->tpl_vars['identificationCodes']->value && !$_smarty_tpl->tpl_vars['publicationDates']->value && !$_smarty_tpl->tpl_vars['hasPubId']->value && !$_smarty_tpl->tpl_vars['publicationFormat']->value->getPhysicalFormat()) {?>
				<?php continue 1;?>
				<?php }?>

				<div class="item publication_format">

										<?php if (count($_smarty_tpl->tpl_vars['publicationFormats']->value) > 1) {?>
					<h2 class="pkp_screen_reader">
						<?php $_smarty_tpl->_assignInScope('publicationFormatName', $_smarty_tpl->tpl_vars['publicationFormat']->value->getLocalizedName());?>
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"monograph.publicationFormatDetails",'format'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationFormatName']->value ))),$_smarty_tpl ) );?>

					</h2>

					<div class="sub_item item_heading format">
						<div class="label">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationFormat']->value->getLocalizedName() ));?>

						</div>
					</div>
					<?php } else { ?>
					<h2 class="pkp_screen_reader">
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"monograph.miscellaneousDetails"),$_smarty_tpl ) );?>

					</h2>
					<?php }?>


										<?php if ($_smarty_tpl->tpl_vars['identificationCodes']->value) {?>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['identificationCodes']->value, 'identificationCode');
$_smarty_tpl->tpl_vars['identificationCode']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['identificationCode']->value) {
$_smarty_tpl->tpl_vars['identificationCode']->do_else = false;
?>
					<div class="sub_item identification_code">
						<h3 class="label">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['identificationCode']->value->getNameForONIXCode() ));?>

						</h3>
						<div class="value">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['identificationCode']->value->getValue() ));?>

						</div>
					</div>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					<?php }?>

										<?php if ($_smarty_tpl->tpl_vars['publicationDates']->value) {?>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['publicationDates']->value, 'publicationDate');
$_smarty_tpl->tpl_vars['publicationDate']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['publicationDate']->value) {
$_smarty_tpl->tpl_vars['publicationDate']->do_else = false;
?>
					<div class="sub_item date">
						<h3 class="label">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationDate']->value->getNameForONIXCode() ));?>

						</h3>
						<div class="value">
							<?php $_smarty_tpl->_assignInScope('dates', $_smarty_tpl->tpl_vars['publicationDate']->value->getReadableDates());?>
														<?php if ($_smarty_tpl->tpl_vars['publicationDate']->value->isFreeText() || count($_smarty_tpl->tpl_vars['dates']->value) == 1) {?>
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['dates']->value[0] ));?>

							<?php } else { ?>
														<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['dates']->value[0] ));?>
&mdash;<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['dates']->value[1] ));?>

							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['publicationDate']->value->isHijriCalendar()) {?>
							<div class="hijri">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.dateHijri"),$_smarty_tpl ) );?>

							</div>
							<?php }?>
						</div>
					</div>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					<?php }?>

										<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pubIdPlugins']->value, 'pubIdPlugin');
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pubIdPlugin']->value) {
$_smarty_tpl->tpl_vars['pubIdPlugin']->do_else = false;
?>
					<?php $_smarty_tpl->_assignInScope('pubIdType', $_smarty_tpl->tpl_vars['pubIdPlugin']->value->getPubIdType());?>
					<?php $_smarty_tpl->_assignInScope('storedPubId', $_smarty_tpl->tpl_vars['publicationFormat']->value->getStoredPubId($_smarty_tpl->tpl_vars['pubIdType']->value));?>
					<?php if ($_smarty_tpl->tpl_vars['storedPubId']->value != '') {?>
					<div class="sub_item pubid <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationFormat']->value->getId() ));?>
">
						<h2 class="label">
							<?php echo $_smarty_tpl->tpl_vars['pubIdType']->value;?>

						</h2>
						<div class="value">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['storedPubId']->value ));?>

						</div>
					</div>
					<?php }?>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

										<?php if ($_smarty_tpl->tpl_vars['publicationFormat']->value->getPhysicalFormat()) {?>
					<div class="sub_item dimensions">
						<h2 class="label">
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"monograph.publicationFormat.productDimensions"),$_smarty_tpl ) );?>

						</h2>
						<div class="value">
							<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publicationFormat']->value->getDimensions() ));?>

						</div>
					</div>
					<?php }?>
				</div>
				<?php }?>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				<?php }?>

			</div>
		</div>


		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"series.series"),$_smarty_tpl ) );?>
</h4>
				<h6><button class="btn btn-info" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>" catalog",'op'=>"series",'path'=>$_smarty_tpl->tpl_vars['series']->value->getPath()),$_smarty_tpl ) );?>
">
						<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['series']->value->getLocalizedFullTitle() ));?>

					</button></h6>
				<?php if ($_smarty_tpl->tpl_vars['series']->value->getOnlineISSN()) {?>
				<h6><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['series']->value->getOnlineISSN() ));?>
</h6>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['series']->value->getPrintISSN()) {?>
				<h6><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['series']->value->getPrintISSN() ));?>
</h6>
				<?php }?>
			</div>
		</div>

		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>Categorias</h4>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category');
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>

				<button class="btn btn-success" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('op'=>" category",'path'=>$_smarty_tpl->tpl_vars['category']->value->getPath()),$_smarty_tpl ) );?>
">
					<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'strip_unsafe_html' ][ 0 ], array( $_smarty_tpl->tpl_vars['category']->value->getLocalizedTitle() ));?>

				</button>

				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</div>
		</div>

		<?php if ($_smarty_tpl->tpl_vars['publication']->value->getData('licenseUrl')) {?>
		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>Licencia</h4>
				<?php if ($_smarty_tpl->tpl_vars['ccLicenseBadge']->value) {?>
				<?php echo $_smarty_tpl->tpl_vars['ccLicenseBadge']->value;?>

				<?php } else { ?>
				<a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['publication']->value->getData('licenseUrl') ));?>
">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.license"),$_smarty_tpl ) );?>

				</a>
				<?php }?>
			</div>
		</div>
		<?php }?>



		<?php if ($_smarty_tpl->tpl_vars['publication']->value->getData('copyrightYear') && $_smarty_tpl->tpl_vars['publication']->value->getLocalizedData('copyrightHolder')) {?>
		<div class="col-12 col-md-6 order-1 order-md-2">
			<div class="bg-light text-center p-2">
				<h4>Copyright</h4>
				<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.copyrightStatement",'copyrightYear'=>$_smarty_tpl->tpl_vars['publication']->value->getData('copyrightYear'),'copyrightHolder'=>$_smarty_tpl->tpl_vars['publication']->value->getLocalizedData('copyrightHolder')),$_smarty_tpl ) ) ));?>

			</div>
		</div>
		<?php }?>







	</div>
</div><?php }
}
