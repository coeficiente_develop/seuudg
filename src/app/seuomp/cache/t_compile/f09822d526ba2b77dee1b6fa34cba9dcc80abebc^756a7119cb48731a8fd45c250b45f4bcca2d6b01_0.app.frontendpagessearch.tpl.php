<?php
/* Smarty version 3.1.39, created on 2022-03-14 18:53:57
  from 'app:frontendpagessearch.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_622f8f45109b74_69592121',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '756a7119cb48731a8fd45c250b45f4bcca2d6b01' => 
    array (
      0 => 'app:frontendpagessearch.tpl',
      1 => 1647284034,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/header.tpl' => 1,
    'app:frontend/components/searchForm_simple.tpl' => 1,
    'app:frontend/objects/monograph_summary.tpl' => 1,
    'app:frontend/components/footer.tpl' => 1,
  ),
),false)) {
function content_622f8f45109b74_69592121 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("app:frontend/components/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pageTitle'=>"common.search"), 0, false);
?>

<br>

    <div class="row">
          <div class="col-sm-12">
            <center>
              <img src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/UDG640x640.png" style="width:80px;height:100px;" width="100">
            </center>
          </div>
    </div>
  	<div class="row">
      <div class="col-sm-12">
        <h1  class="text-center" style="color:White;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.search"),$_smarty_tpl ) );?>
</h1>
      </div>
    </div>
    <div class="row"><br> </div>
    <a name="search-form"></a>
    <?php $_smarty_tpl->_subTemplateRender("app:frontend/components/searchForm_simple.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <div class="row"><br><br> <hr style="color:White;"> </div>
    <div class="row">
      <div class="col-sm-12">
  		   <h4 class="text-center" style="color:White;">
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"catalog.browseTitles",'numTitles'=>$_smarty_tpl->tpl_vars['results']->value->getCount()),$_smarty_tpl ) );?>

          </h4>
      </div>
  	</div>
    <div class="row"><br></div>

  	<?php if ($_smarty_tpl->tpl_vars['searchQuery']->value == '') {?>


  	<?php } elseif ($_smarty_tpl->tpl_vars['results']->value->getCount() == 0) {?>
    <div class="row">
      <div class="col-sm-12">
         <h4 class="text-center" style="color:White;">
           	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"catalog.noTitlesSearch",'searchQuery'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['searchQuery']->value ))),$_smarty_tpl ) );?>

          </h4>
      </div>
    </div>
    <div class="row"> <br> <hr></div>
  	<?php } else { ?>
    <div class="row">
      <div class="col-sm-12">
         <div class="text-center" style="color:White;">
           <div>
             <?php if ($_smarty_tpl->tpl_vars['results']->value->getCount() > 1) {?>
               <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"catalog.foundTitlesSearch",'searchQuery'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['searchQuery']->value )),'number'=>$_smarty_tpl->tpl_vars['results']->value->getCount()),$_smarty_tpl ) );?>

             <?php } else { ?>
               <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"catalog.foundTitleSearch",'searchQuery'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['searchQuery']->value ))),$_smarty_tpl ) );?>

             <?php }?>
             <a href="#search-form">
               <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"search.searchAgain"),$_smarty_tpl ) );?>

             </a>
           </div>
           <div>
             <?php $_smarty_tpl->_assignInScope('counter', 1);?>
             <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['iterate'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['iterate'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'smartyIterate'))) {
throw new SmartyException('block tag \'iterate\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('iterate', array('from'=>'results','item'=>'result'));
$_block_repeat=true;
echo $_block_plugin1->smartyIterate(array('from'=>'results','item'=>'result'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
               <?php if ((1 & $_smarty_tpl->tpl_vars['counter']->value / 1)) {?>
                 <div >
               <?php }?>
                 <?php $_smarty_tpl->_subTemplateRender("app:frontend/objects/monograph_summary.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('monograph'=>$_smarty_tpl->tpl_vars['result']->value['publishedSubmission'],'press'=>$_smarty_tpl->tpl_vars['result']->value['press'],'heading'=>"h2"), 0, false);
?>
               <?php if (!(1 & $_smarty_tpl->tpl_vars['counter']->value / 1)) {?>
                 </div>
               <?php }?>
               <?php $_smarty_tpl->_assignInScope('counter', $_smarty_tpl->tpl_vars['counter']->value+1);?>
             <?php $_block_repeat=false;
echo $_block_plugin1->smartyIterate(array('from'=>'results','item'=>'result'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                          <?php if ($_smarty_tpl->tpl_vars['counter']->value > 1 && !(1 & $_smarty_tpl->tpl_vars['counter']->value / 1)) {?>
               </div>
             <?php }?>
             <div class="">
               <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['page_info'][0], array( array('iterator'=>$_smarty_tpl->tpl_vars['results']->value),$_smarty_tpl ) );?>

               <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['page_links'][0], array( array('anchor'=>"results",'iterator'=>$_smarty_tpl->tpl_vars['results']->value,'name'=>"search",'query'=>$_smarty_tpl->tpl_vars['searchQuery']->value),$_smarty_tpl ) );?>

             </div>

           </div>
         </div>
      </div>
    
    

  	<?php }?>

<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
