<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:54:56
  from 'app:frontendcomponentsfooter.' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234ab50a4c226_42369733',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4dffb64063bb972c37e05619a2ccd9d0ea7473ac' => 
    array (
      0 => 'app:frontendcomponentsfooter.',
      1 => 1647611248,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6234ab50a4c226_42369733 (Smarty_Internal_Template $_smarty_tpl) {
?>
	

	




		<footer class="bg-dark text-white text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">

                    <img class="img-fluid" id="imgfooter" src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/UDG640x640.png" style="width:80px;height:100px;" class="img-fluid">

          <!-- <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul> -->
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">


          <h6>Universidad de Guadalajara
            <br>
            <br>
            Av.Juarez No.976
            <br>
             Colonia Centro C.P 44100 Guadalajara, Jalisco, Mexico
             <br>
            Telefono +52 (33) 3134 2297
            <br>
             Ext 12297
          </h6>

          <!-- <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul> -->
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          

                  </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-white text-uppercase mb-0">Redes Sociales</h5>

          <!-- Facebook -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #3b5998;"
href="https://www.facebook.com/udg.mx"
role="button"
><i class="bi bi-facebook m-auto"></i
></a>

<!-- Twitter -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #55acee;"
href="https://twitter.com/udg_oficial"
role="button"
><i class="bi bi-twitter m-auto"></i
></a>



<!-- Instagram -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #ac2bac;"
href="https://www.instagram.com/udg_oficial/"
role="button"
><i class="bi bi-instagram m-auto"></i
></a>

<!-- Linkedin -->
<a
class="btn btn-primary btn-floating m-1"
style="background-color: #0082ca;"
href="https://www.linkedin.com/school/universidad-de-guadalajara_2/"
role="button"
><i class="bi-linkedin m-auto"></i
></a>

        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->

          

    <div class="text-center text-white p-3" style="background-color: rgba(0, 0, 0, 0.2);">
      
     
          <ul class="list-unstyled mb-0">
            <li>
              <h5 class="text-white"> Derechos reservados©2022</h5>
            </li>
            <li>
                          </li>
            <li>
              <a href="https://www.coeficiente.mx/" class="text-white">COEFICIENTE</a>
            </li>
            
          </ul>
    </div>
    <!-- Copyright -->
  </footer>

		
	




<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['load_script'][0], array( array('context'=>"frontend"),$_smarty_tpl ) );?>



</body>
</html>
<?php }
}
