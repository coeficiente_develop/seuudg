<?php
/* Smarty version 3.1.39, created on 2022-03-09 19:09:02
  from 'app:frontendpagesuserLogin.tp' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6228fb4e8976f9_58915797',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bd04826a47a63ab4c25ca20ffc3b42835fcab481' => 
    array (
      0 => 'app:frontendpagesuserLogin.tp',
      1 => 1646852925,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/header.tpl' => 1,
    'app:frontend/components/footer.tpl' => 1,
  ),
),false)) {
function content_6228fb4e8976f9_58915797 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("app:frontend/components/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pageTitle'=>"user.login"), 0, false);
?>


    <div class="container mt-5 mb-5">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-6">
                <div class="card bg-dark p-3 text-center text-white">
				
				

				<input type="hidden" name="source" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['source']->value ));?>
" />
                    
					<form id="login" method="post" action="<?php echo $_smarty_tpl->tpl_vars['loginUrl']->value;?>
">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['csrf'][0], array( array(),$_smarty_tpl ) );?>

                    
                    
                    <div> <img src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/logo_udg_sf.png" width="100"> </div>
                    <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
               
                   <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>$_smarty_tpl->tpl_vars['error']->value,'reason'=>$_smarty_tpl->tpl_vars['reason']->value),$_smarty_tpl ) );?>

               
                <?php }?>

				<?php if ($_smarty_tpl->tpl_vars['loginMessage']->value) {?>
    		<h6>
    			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>$_smarty_tpl->tpl_vars['loginMessage']->value),$_smarty_tpl ) );?>

    		</h6>
    	        <?php }?>
                    <h1>Bienvenido</h1>
                    <div class="p-2 px-5"> 
                        <input class="form-control" type="text" name="username" id="username" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['username']->value ));?>
" maxlength="32" required aria-required="true" autocomplete="username" placeholder="Usuario">
                        <input type="password" class="form-control" name="password" id="password" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['password']->value ));?>
" password="true" maxlength="32" required aria-required="true" autocomplete="current-password" placeholder="Contraseña">
                        <hr>
                         <button class="btn btn-danger w-100 signup-button" type="submit">Iniciar Sesion</button>
						 <hr>
                         <label><input type="checkbox" name="remember" id="remember" value="1" checked="$remember"> Mantenerse Conectado</label><br>
                        
                        <hr> 
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

                    </form>
    
	        			<?php if ($_smarty_tpl->tpl_vars['recaptchaPublicKey']->value) {?>
        				<fieldset class="recaptcha_wrapper">
        					<div class="fields">
        						<div class="recaptcha">
        							<div class="g-recaptcha" data-sitekey="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['recaptchaPublicKey']->value ));?>
">
        							</div>
        						</div>
        					</div>
        				</fieldset>
        			<?php }?>

<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
