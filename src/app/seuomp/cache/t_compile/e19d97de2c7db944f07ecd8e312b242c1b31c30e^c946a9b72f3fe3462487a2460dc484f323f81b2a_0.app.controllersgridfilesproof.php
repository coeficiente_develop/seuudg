<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:13:18
  from 'app:controllersgridfilesproof' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a18ee05009_62672912',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c946a9b72f3fe3462487a2460dc484f323f81b2a' => 
    array (
      0 => 'app:controllersgridfilesproof',
      1 => 1647574005,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6234a18ee05009_62672912 (Smarty_Internal_Template $_smarty_tpl) {
$_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['fbvFormSection'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['fbvFormSection'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'smartyFBVFormSection'))) {
throw new SmartyException('block tag \'fbvFormSection\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('fbvFormSection', array('for'=>"priceType",'list'=>true,'description'=>"payment.directSales.price.description"));
$_block_repeat=true;
echo $_block_plugin2->smartyFBVFormSection(array('for'=>"priceType",'list'=>true,'description'=>"payment.directSales.price.description"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['salesTypes']->value, 'salesType', false, 'salesTypeKey');
$_smarty_tpl->tpl_vars['salesType']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['salesTypeKey']->value => $_smarty_tpl->tpl_vars['salesType']->value) {
$_smarty_tpl->tpl_vars['salesType']->do_else = false;
?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['fbvElement'][0], array( array('type'=>"radio",'name'=>"salesType",'value'=>$_smarty_tpl->tpl_vars['salesTypeKey']->value,'label'=>$_smarty_tpl->tpl_vars['salesType']->value,'id'=>$_smarty_tpl->tpl_vars['salesTypeKey']->value),$_smarty_tpl ) );?>

	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
$_block_repeat=false;
echo $_block_plugin2->smartyFBVFormSection(array('for'=>"priceType",'list'=>true,'description'=>"payment.directSales.price.description"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php $_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['fbvFormSection'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['fbvFormSection'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'smartyFBVFormSection'))) {
throw new SmartyException('block tag \'fbvFormSection\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('fbvFormSection', array('for'=>"price"));
$_block_repeat=true;
echo $_block_plugin3->smartyFBVFormSection(array('for'=>"price"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
	<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "priceLabel", null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"payment.directSales.priceCurrency",'currency'=>$_smarty_tpl->tpl_vars['currentPress']->value->getSetting('currency')),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['fbvElement'][0], array( array('type'=>"text",'id'=>"price",'label'=>$_smarty_tpl->tpl_vars['priceLabel']->value,'subLabelTranslate'=>false,'size'=>$_smarty_tpl->tpl_vars['fbvStyles']->value['size']['MEDIUM'],'value'=>$_smarty_tpl->tpl_vars['price']->value,'maxlength'=>"255"),$_smarty_tpl ) );?>

	<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"payment.directSales.numericOnly"),$_smarty_tpl ) );?>
</p>
<?php $_block_repeat=false;
echo $_block_plugin3->smartyFBVFormSection(array('for'=>"price"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
