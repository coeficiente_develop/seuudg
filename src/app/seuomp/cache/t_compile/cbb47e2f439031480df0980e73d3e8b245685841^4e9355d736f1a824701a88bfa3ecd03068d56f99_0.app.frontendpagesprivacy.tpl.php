<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:54:56
  from 'app:frontendpagesprivacy.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234ab50a18b34_20792233',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4e9355d736f1a824701a88bfa3ecd03068d56f99' => 
    array (
      0 => 'app:frontendpagesprivacy.tpl',
      1 => 1647573533,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/header.tpl' => 1,
    'app:frontend/components/footer.tpl' => 1,
  ),
),false)) {
function content_6234ab50a18b34_20792233 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("app:frontend/components/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pageTitle'=>"manager.setup.privacyStatement"), 0, false);
?>

<header class="masthead">
    <div class="container position-relative">
        <div class="row justify-content-center">
            <div class="col-xl-6">
                <div class="text-center text-white">
                    <!-- Page heading-->
                    <h1 class="mb-5">Sistema Editorial Universitario</h1>
                    <button class="btn btn-primary"><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"catalog"),$_smarty_tpl ) );?>
">Ver Monografias</a></button>
                    <!-- Signup form-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- * * SB Forms Contact Form * *-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- This form is pre-integrated with SB Forms.-->
                    <!-- To make this form functional, sign up at-->
                    <!-- https://startbootstrap.com/solution/contact-forms-->
                    <!-- to get an API token!-->
                    
                </div>
            </div>
        </div>
    </div>
</header>

<section class="features-icons bg-dark text-center text-white">
    <h1><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"manager.setup.privacyStatement"),$_smarty_tpl ) );?>
</h1>
    <div class="container">
        <div class="row">
            <h4><?php echo $_smarty_tpl->tpl_vars['privacyStatement']->value;?>
</h4>
        </div>
    </div>
</section>

<section class="features-icons bg-light text-center">
            <h2>Sitios de Interes</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-gear-wide-connected m-auto text-primary"></i></div>
                            <h6>COORDINACION GENERAL DE INVESTIGACION,POSGRADO Y VINCULACION</h6>
                            <!-- <p class="lead mb-0">Ven y publica con nosotros</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-bank m-auto text-primary"></i></div>
                            <h5>
                            <a class="text-dark" href="https://www.udg.mx/">UNIVERSIDAD DE GUADALAJARA</a>
                            
                            </h5>
                            <!-- <p class="lead mb-0">Por favor lea los requisitos antes de solicitar</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi-journals m-auto text-primary"></i></div>
                            <h5>
                            <a class="text-dark" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"catalog"),$_smarty_tpl ) );?>
">
                            Catalogo
                            </a>
                            </h5>
                            <!-- <p class="lead mb-0">Por favor consulte el reglamento editorial</p> -->
                        </div>
                    </div>
                </div>
            </div>
</section>

<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
