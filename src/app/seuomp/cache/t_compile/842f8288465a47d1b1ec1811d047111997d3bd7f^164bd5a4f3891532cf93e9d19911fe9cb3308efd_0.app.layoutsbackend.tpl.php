<?php
/* Smarty version 3.1.39, created on 2022-03-22 17:21:51
  from 'app:layoutsbackend.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_623a05af604ab0_03105982',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '164bd5a4f3891532cf93e9d19911fe9cb3308efd' => 
    array (
      0 => 'app:layoutsbackend.tpl',
      1 => 1647969643,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:controllers/notification/notificationOptions.tpl' => 1,
  ),
),false)) {
function content_623a05af604ab0_03105982 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/app/seuomp/lib/pkp/lib/vendor/smarty/smarty/libs/plugins/modifier.replace.php','function'=>'smarty_modifier_replace',),));
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html lang="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['currentLocale']->value,"_","-");?>
" xml:lang="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['currentLocale']->value,"_","-");?>
">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['defaultCharset']->value ));?>
" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="http://172.31.99.385:8008/app/seuomp/favicon.ico" type="image/x-icon">

	<title><?php echo preg_replace('!<[^>]*?>!', ' ', call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['title'][0], array( array('value'=>$_smarty_tpl->tpl_vars['pageTitle']->value),$_smarty_tpl ) ));?>
</title>
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['load_header'][0], array( array('context'=>"backend"),$_smarty_tpl ) );?>

	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['load_stylesheet'][0], array( array('context'=>"backend"),$_smarty_tpl ) );?>

	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['load_script'][0], array( array('context'=>"backend"),$_smarty_tpl ) );?>

    <link href="http://10.21.1.149:8008/app/seuomp/udg_resources/bootstrap-5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"  >
    <?php echo '<script'; ?>
 src="http://10.21.1.149:8008/app/seuomp/udg_resources/bootstrap-5.1.3/dist/js/bootstrap.bundle.min.js" ><?php echo '</script'; ?>
>


</head>
<body class="pkp_page_<?php echo (($tmp = @call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['requestedPage']->value )))===null||$tmp==='' ? "index" : $tmp);?>
 pkp_op_<?php echo (($tmp = @call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['requestedOp']->value )))===null||$tmp==='' ? "index" : $tmp);?>
" dir="<?php echo (($tmp = @call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['currentLocaleLangDir']->value )))===null||$tmp==='' ? "ltr" : $tmp);?>
" style="background:#F3EFDC">

	<?php echo '<script'; ?>
 type="text/javascript">
		// Initialise JS handler.
		$(function() {
			$('body').pkpHandler(
				'$.pkp.controllers.SiteHandler',
				{
					<?php $_smarty_tpl->_subTemplateRender("app:controllers/notification/notificationOptions.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
				});
		});
	<?php echo '</script'; ?>
>

	<div id="app" class="app <?php if ($_smarty_tpl->tpl_vars['isLoggedInAs']->value) {?> app--isLoggedInAs<?php }?>">
		<header class="" role="banner">
			<nav class="navbar navbar-expand-lg navbar-light bg-dark">

				<?php if ($_smarty_tpl->tpl_vars['availableContexts']->value) {?>
					<dropdown class="app__headerAction app__contexts" v-cloak>
						<template slot="button">
							<icon icon="sitemap"></icon>
							<span class="-screenReader"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"context.contexts"),$_smarty_tpl ) );?>
====</span>
						</template>
						<ul>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableContexts']->value, 'availableContext');
$_smarty_tpl->tpl_vars['availableContext']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['availableContext']->value) {
$_smarty_tpl->tpl_vars['availableContext']->do_else = false;
?>
								<?php if (!$_smarty_tpl->tpl_vars['currentContext']->value || $_smarty_tpl->tpl_vars['availableContext']->value->name !== $_smarty_tpl->tpl_vars['currentContext']->value->getLocalizedData('name')) {?>
									<li>
										<a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['availableContext']->value->url ));?>
" class="pkpDropdown__action">
											<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['availableContext']->value->name ));?>

										</a>
									</li>
								<?php }?>
							<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						</ul>
					</dropdown>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['currentContext']->value) {?>
					<a class="app__contextTitle" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"index"),$_smarty_tpl ) );?>
">
						<img id="imgbanner" class="img-fluid" src="http://10.21.1.149:8008/app/seuomp/udg_resources/assets/img/UDG640x640.png"  style="width:50px;height:60px;"/>
					</a>
				<?php } elseif ($_smarty_tpl->tpl_vars['siteTitle']->value) {?>
					<a class="app__contextTitle" href="<?php echo $_smarty_tpl->tpl_vars['baseUrl']->value;?>
">
					<?php echo $_smarty_tpl->tpl_vars['siteTitle']->value;?>

					</a>
				<?php } else { ?>
					<div class="app__contextTitle">
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.software"),$_smarty_tpl ) );?>

					</div>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['currentUser']->value) {?>
					<div class="app__headerActions" v-cloak>
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['call_hook'][0], array( array('name'=>"Template::Layout::Backend::HeaderActions"),$_smarty_tpl ) );?>

						<div class="app__headerAction app__tasks">
							<button ref="tasksButton" @click="openTasks">
								<icon icon="bell-o"></icon>
								<span class="-screenReader"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.tasks"),$_smarty_tpl ) );?>
</span>
								<span v-if="unreadTasksCount" class="app__tasksCount">{{ unreadTasksCount }}</span>
							</button>
						</div>
						<dropdown class="app__headerAction app__userNav">
							<template slot="button">
								<icon icon="user-circle-o"></icon>
								<?php if ($_smarty_tpl->tpl_vars['isUserLoggedInAs']->value) {?>
									<icon icon="user-circle" class="app__userNav__isLoggedInAsWarning"></icon>
								<?php }?>
								<span class="-screenReader"><?php echo $_smarty_tpl->tpl_vars['currentUser']->value->getData('username');?>
</span>
							</template>
							<nav aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.navigation.user"),$_smarty_tpl ) );?>
">
								<?php if (count($_smarty_tpl->tpl_vars['supportedLocales']->value) > 1) {?>
									<div class="pkpDropdown__section">
										<div class="app__userNav__changeLocale">Cambiar Idioma</div>
										<ul>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['supportedLocales']->value, 'locale', false, 'localeKey');
$_smarty_tpl->tpl_vars['locale']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['localeKey']->value => $_smarty_tpl->tpl_vars['locale']->value) {
$_smarty_tpl->tpl_vars['locale']->do_else = false;
?>
												<li>
													<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_PAGE') ? constant('ROUTE_PAGE') : null),'page'=>"user",'op'=>"setLocale",'path'=>$_smarty_tpl->tpl_vars['localeKey']->value),$_smarty_tpl ) );?>
" class="pkpDropdown__action">
														<?php if ($_smarty_tpl->tpl_vars['localeKey']->value == $_smarty_tpl->tpl_vars['currentLocale']->value) {?>
															<icon icon="check" :inline="true"></icon>
														<?php }?>

														<?php echo $_smarty_tpl->tpl_vars['locale']->value;?>



													</a>
												</li>
											<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
										</ul>
									</div>
								<?php }?>
								<?php if ($_smarty_tpl->tpl_vars['isUserLoggedInAs']->value) {?>
									<div class="pkpDropdown__section">
										<div class="app__userNav__loggedInAs">
											<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"manager.people.signedInAs",'username'=>$_smarty_tpl->tpl_vars['currentUser']->value->getData('username')),$_smarty_tpl ) );?>

											<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_PAGE') ? constant('ROUTE_PAGE') : null),'page'=>"login",'op'=>"signOutAsUser"),$_smarty_tpl ) );?>
" class="app__userNav__logOutAs"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"user.logOutAs",'username'=>$_smarty_tpl->tpl_vars['currentUser']->value->getData('username')),$_smarty_tpl ) );?>
</a>.
										</div>
									</div>
								<?php }?>
								<div class="pkpDropdown__section">
									<ul>
										<li v-if="backToDashboardLink">
											<a :href="backToDashboardLink.url" class="pkpDropdown__action">
												{{ backToDashboardLink.name }}
											</a>
										</li>
										<li>
											<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_PAGE') ? constant('ROUTE_PAGE') : null),'page'=>"user",'op'=>"profile"),$_smarty_tpl ) );?>
" class="pkpDropdown__action">
												<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"user.profile.editProfile"),$_smarty_tpl ) );?>

											</a>
										</li>
										<li>
											<?php if ($_smarty_tpl->tpl_vars['isUserLoggedInAs']->value) {?>
												<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_PAGE') ? constant('ROUTE_PAGE') : null),'page'=>"login",'op'=>"signOutAsUser"),$_smarty_tpl ) );?>
" class="pkpDropdown__action">
													<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"user.logOutAs",'username'=>$_smarty_tpl->tpl_vars['currentUser']->value->getData('username')),$_smarty_tpl ) );?>

												</a>
											<?php } else { ?>
												<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_PAGE') ? constant('ROUTE_PAGE') : null),'page'=>"login",'op'=>"signOut"),$_smarty_tpl ) );?>
" class="pkpDropdown__action">
													<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"user.logOut"),$_smarty_tpl ) );?>

												</a>
											<?php }?>
										</li>
									</ul>
								</div>
							</nav>
						</dropdown>
					</div>
				<?php }?>
            </nav>

		</header>

    	<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_773729721623a05af5fce90_36955685', "menu");
?>

		
				<nav v-if="backToDashboardLink" class="app__returnHeader" aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.navigation.site"),$_smarty_tpl ) );?>
">
			<a class="app__returnHeaderLink" :href="backToDashboardLink.url">
				{{ backToDashboardLabel }}
			</a>
		</nav>
		<br>
		<div class="container bg-white"> 
			<main class="">
				<div class="">
					<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_346928381623a05af5feb55_25651081', "breadcrumbs");
?>

					<hr>
										<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_574153218623a05af5fff25_55836852', "page");
?>


				</div>
			</main>
		</div>
		<div
			aria-live="polite"
			aria-atomic="true"
			class="app__notifications"
			ref="notifications"
			role="status"
		>
			<transition-group name="app__notification">
				<notification v-for="notification in notifications" :key="notification.key" :type="notification.type" :can-dismiss="true" @dismiss="dismissNotification(notification.key)">
									</notification>
			</transition-group>
		</div>
	</div>

	<?php echo '<script'; ?>
 type="text/javascript">
		pkp.registry.init('app', <?php echo json_encode($_smarty_tpl->tpl_vars['pageComponent']->value);?>
, <?php echo json_encode($_smarty_tpl->tpl_vars['state']->value);?>
);
	<?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
 type="text/javascript">
		// Initialize JS handler
		$(function() {
			$('#pkpHelpPanel').pkpHandler(
				'$.pkp.controllers.HelpPanelHandler',
				{
					helpUrl: <?php echo json_encode(call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('page'=>"help",'escape'=>false),$_smarty_tpl ) ));?>
,
					helpLocale: '<?php echo substr($_smarty_tpl->tpl_vars['currentLocale']->value,0,2);?>
',
				}
			);
		});
	<?php echo '</script'; ?>
>
	
	
</body>
</html>
<?php }
/* {block "menu"} */
class Block_773729721623a05af5fce90_36955685 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'menu' => 
  array (
    0 => 'Block_773729721623a05af5fce90_36955685',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

		    <div v-if="!!menu && Object.keys(menu).length > 1"  aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.navigation.site"),$_smarty_tpl ) );?>
" >
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<div class="container-fluid">
					<a class="navbar-brand" href="htt172.31.99.38.1.75:8008/app/seuomp/index.php/editorialudg/index">SEU</a>
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
						<div class="collapse navbar-collapse" id="navbarNavDropdown">
							<ul class="navbar-nav">                 
								<li  v-for="(menuItem, key) in menu" :key="key" :class="!!menuItem.submenu ? 'nav-item dropdown' : 'nav-item '"  >
									<div v-if="!!menuItem.submenu" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
										<a>
										{{ menuItem.name }}
										</a>
									</div>
									<a v-else-if="key !== 'tools' && key !== 'admin'"  :class="menuItem.isCurrent ? 'nav-link' : 'nav-link'" :href="menuItem.url"  >
										{{ menuItem.name }}
									</a>						
									<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<div  v-if="!!menuItem.submenu" >
										<li v-for="(submenuItem, submenuKey) in menuItem.submenu" :key="submenuKey">									
											<a v-if="submenuKey !== 'website'"  :class="submenuItem.isCurrent ? 'dropdown-item' : 'dropdown-item'" :href="submenuItem.url">
												{{ submenuItem.name }}									
											</a>
										</li>  
										</div>                          
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		<?php
}
}
/* {/block "menu"} */
/* {block "breadcrumbs"} */
class Block_346928381623a05af5feb55_25651081 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'breadcrumbs' => 
  array (
    0 => 'Block_346928381623a05af5feb55_25651081',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

						<?php if ($_smarty_tpl->tpl_vars['breadcrumbs']->value) {?>
							<nav class="app__breadcrumbs" role="navigation" aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"navigation.breadcrumbLabel"),$_smarty_tpl ) );?>
">
								<ol>
																	</ol>
							</nav>
						<?php }?>
					<?php
}
}
/* {/block "breadcrumbs"} */
/* {block "page"} */
class Block_574153218623a05af5fff25_55836852 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page' => 
  array (
    0 => 'Block_574153218623a05af5fff25_55836852',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "page"} */
}
