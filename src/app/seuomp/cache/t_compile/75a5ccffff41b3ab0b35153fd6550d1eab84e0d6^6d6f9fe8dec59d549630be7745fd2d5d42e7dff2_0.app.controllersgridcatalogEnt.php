<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:12:53
  from 'app:controllersgridcatalogEnt' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a1756b4b87_85930513',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6d6f9fe8dec59d549630be7745fd2d5d42e7dff2' => 
    array (
      0 => 'app:controllersgridcatalogEnt',
      1 => 1647574002,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6234a1756b4b87_85930513 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
	// Attach the JS file tab handler.
	$(function() {
		$('#editPublicationFormatMetadataTabs').pkpHandler('$.pkp.controllers.TabHandler');
	});
<?php echo '</script'; ?>
>
<div id="editPublicationFormatMetadataTabs">
	<ul>
		<li><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_COMPONENT') ? constant('ROUTE_COMPONENT') : null),'op'=>"editFormatTab",'submissionId'=>$_smarty_tpl->tpl_vars['submissionId']->value,'representationId'=>$_smarty_tpl->tpl_vars['representationId']->value,'publicationId'=>$_smarty_tpl->tpl_vars['publicationId']->value),$_smarty_tpl ) );?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"common.edit"),$_smarty_tpl ) );?>
</a></li>
		<li><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_COMPONENT') ? constant('ROUTE_COMPONENT') : null),'op'=>"editFormatMetadata",'submissionId'=>$_smarty_tpl->tpl_vars['submissionId']->value,'representationId'=>$_smarty_tpl->tpl_vars['representationId']->value,'publicationId'=>$_smarty_tpl->tpl_vars['publicationId']->value),$_smarty_tpl ) );?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.informationCenter.metadata"),$_smarty_tpl ) );?>
</a></li>
		<?php if (!$_smarty_tpl->tpl_vars['remoteRepresentation']->value && $_smarty_tpl->tpl_vars['representationId']->value) {?>
			<?php if ($_smarty_tpl->tpl_vars['showIdentifierTab']->value) {?>
				<li><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_COMPONENT') ? constant('ROUTE_COMPONENT') : null),'op'=>"identifiers",'submissionId'=>$_smarty_tpl->tpl_vars['submissionId']->value,'representationId'=>$_smarty_tpl->tpl_vars['representationId']->value,'publicationId'=>$_smarty_tpl->tpl_vars['publicationId']->value),$_smarty_tpl ) );?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"submission.identifiers"),$_smarty_tpl ) );?>
</a></li>
			<?php }?>
		<?php }?>
	</ul>
</div>
<?php }
}
