<?php
/* Smarty version 3.1.39, created on 2022-03-16 16:01:28
  from 'app:frontendpagescontact.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_623209d8d0ee11_78448605',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6b0444a34e5318c94e752093d374a9114c1ff855' => 
    array (
      0 => 'app:frontendpagescontact.tpl',
      1 => 1646851890,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:frontend/components/header.tpl' => 1,
    'app:frontend/components/footer.tpl' => 1,
  ),
),false)) {
function content_623209d8d0ee11_78448605 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/app/seuomp/lib/pkp/lib/vendor/smarty/smarty/libs/plugins/function.mailto.php','function'=>'smarty_function_mailto',),));
$_smarty_tpl->_subTemplateRender("app:frontend/components/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pageTitle'=>"about.contact"), 0, false);
?>

<header class="masthead">
    <div class="container position-relative">
        <div class="row justify-content-center">
            <div class="col-xl-6">
                <div class="text-center text-white">
                    <!-- Page heading-->
                    <h1 class="mb-5">Sistema Editorial Universitario</h1>
                    <button class="btn btn-primary">Conoce nuestros lineamientos</button>
                    <!-- Signup form-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- * * SB Forms Contact Form * *-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- This form is pre-integrated with SB Forms.-->
                    <!-- To make this form functional, sign up at-->
                    <!-- https://startbootstrap.com/solution/contact-forms-->
                    <!-- to get an API token!-->
                    
                </div>
            </div>
        </div>
    </div>
</header>

<section class="features-icons bg-dark text-center text-white">
    <h1><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"about.contact"),$_smarty_tpl ) );?>
</h1>
    <div class="container">
        <div class="row">
            <h6>
			<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'strip_unsafe_html' ][ 0 ], array( nl2br($_smarty_tpl->tpl_vars['mailingAddress']->value) ));?>

			</h6>
			<h6>
			<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contactName']->value ));?>

			</h6>
			<h6>
			<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contactTitle']->value ));?>

			</h6>
			<h6>
			<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'strip_unsafe_html' ][ 0 ], array( $_smarty_tpl->tpl_vars['contactAffiliation']->value ));?>

			</h6>
			<h6>
			<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contactPhone']->value ));?>

			</h6>
			<h6>
			<?php echo smarty_function_mailto(array('address'=>$_smarty_tpl->tpl_vars['contactEmail']->value,'encode'=>'javascript'),$_smarty_tpl);?>

			</h6>

			<h2>Soporte Tecnico</h2>
			<h6><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['supportName']->value ));?>
</h6>
			<h6><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['supportEmail']->value ));?>
</h6>
			<h6><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['supportPhone']->value ));?>
</h6>
			
			
			
			
			
        </div>
    </div>
</section>

<section class="features-icons bg-light text-center">
    <h2>Sitios de Interes</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-gear-wide-connected m-auto text-primary"></i></div>
                    <h6>COORDINACION GENERAL DE INVESTIGACION,POSGRADO Y VINCULACION</h6>
                    <!-- <p class="lead mb-0">Ven y publica con nosotros</p> -->
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-bank m-auto text-primary"></i></div>
                    <h5>UNIVERSIDAD DE GUADALAJARA</h5>
                    <!-- <p class="lead mb-0">Por favor lea los requisitos antes de solicitar</p> -->
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-journals m-auto text-primary"></i></div>
                    <h5>EDITORIAL UNIVERSIDAD DE GUADALAJARA</h5>
                    <!-- <p class="lead mb-0">Por favor consulte el reglamento editorial</p> -->
                </div>
            </div>
        </div>
    </div>
</section>


<?php $_smarty_tpl->_subTemplateRender("app:frontend/components/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
