<?php
/* Smarty version 3.1.39, created on 2022-03-18 14:58:49
  from 'app:submissionformstep1.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_62349e2945a1a9_86062508',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd4a7db69d40565910b7236a1c96ad325268f4ab6' => 
    array (
      0 => 'app:submissionformstep1.tpl',
      1 => 1647574018,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'app:submission/form/series.tpl' => 1,
    'core:submission/form/step1.tpl' => 1,
  ),
),false)) {
function content_62349e2945a1a9_86062508 (Smarty_Internal_Template $_smarty_tpl) {
?> <?php if ($_smarty_tpl->tpl_vars['currentContext']->value->getData('disableSubmissions')) {?>
	<notification>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"manager.setup.disableSubmissions.notAccepting"),$_smarty_tpl ) );?>

	</notification>
<?php } else { ?>
	<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "additionalFormContent1", null);?>
		<!-- Submission Type -->
		<?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['fbvFormSection'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['fbvFormSection'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'smartyFBVFormSection'))) {
throw new SmartyException('block tag \'fbvFormSection\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('fbvFormSection', array('list'=>"true",'label'=>"submission.workflowType",'description'=>"submission.workflowType.description"));
$_block_repeat=true;
echo $_block_plugin1->smartyFBVFormSection(array('list'=>"true",'label'=>"submission.workflowType",'description'=>"submission.workflowType.description"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['fbvElement'][0], array( array('type'=>"radio",'name'=>"workType",'id'=>"isEditedVolume-0",'value'=>(defined('WORK_TYPE_AUTHORED_WORK') ? constant('WORK_TYPE_AUTHORED_WORK') : null),'checked'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'compare' ][ 0 ], array( $_smarty_tpl->tpl_vars['workType']->value,(defined('WORK_TYPE_EDITED_VOLUME') ? constant('WORK_TYPE_EDITED_VOLUME') : null),false,true )),'label'=>"submission.workflowType.authoredWork",'disabled'=>$_smarty_tpl->tpl_vars['submissionId']->value),$_smarty_tpl ) );?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['fbvElement'][0], array( array('type'=>"radio",'name'=>"workType",'id'=>"isEditedVolume-1",'value'=>(defined('WORK_TYPE_EDITED_VOLUME') ? constant('WORK_TYPE_EDITED_VOLUME') : null),'checked'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'compare' ][ 0 ], array( $_smarty_tpl->tpl_vars['workType']->value,(defined('WORK_TYPE_EDITED_VOLUME') ? constant('WORK_TYPE_EDITED_VOLUME') : null) )),'label'=>"submission.workflowType.editedVolume",'disabled'=>$_smarty_tpl->tpl_vars['submissionId']->value),$_smarty_tpl ) );?>

		<?php $_block_repeat=false;
echo $_block_plugin1->smartyFBVFormSection(array('list'=>"true",'label'=>"submission.workflowType",'description'=>"submission.workflowType.description"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
	<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
	<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "additionalFormContent2", null);?>
		<?php $_smarty_tpl->_subTemplateRender("app:submission/form/series.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('includeSeriesPosition'=>false), 0, false);
?>
	<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>

	<?php $_smarty_tpl->_subTemplateRender("core:submission/form/step1.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
}
