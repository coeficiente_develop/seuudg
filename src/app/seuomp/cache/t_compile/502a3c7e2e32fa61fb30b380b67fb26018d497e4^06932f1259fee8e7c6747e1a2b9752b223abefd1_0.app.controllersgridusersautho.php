<?php
/* Smarty version 3.1.39, created on 2022-03-18 16:30:27
  from 'app:controllersgridusersautho' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234b3a3447151_35594232',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '06932f1259fee8e7c6747e1a2b9752b223abefd1' => 
    array (
      0 => 'app:controllersgridusersautho',
      1 => 1647574009,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'core:controllers/grid/users/author/form/authorForm.tpl' => 1,
  ),
),false)) {
function content_6234b3a3447151_35594232 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['submission']->value->getData('workType') === (defined('WORK_TYPE_EDITED_VOLUME') ? constant('WORK_TYPE_EDITED_VOLUME') : null)) {?>
  <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "additionalCheckboxes", null);?>
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['fbvElement'][0], array( array('type'=>"checkbox",'label'=>"author.isVolumeEditor",'id'=>"isVolumeEditor",'checked'=>$_smarty_tpl->tpl_vars['isVolumeEditor']->value),$_smarty_tpl ) );?>

  <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
}?>

<?php $_smarty_tpl->_subTemplateRender("core:controllers/grid/users/author/form/authorForm.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('additionalCheckboxes'=>$_smarty_tpl->tpl_vars['additionalCheckboxes']->value), 0, false);
}
}
