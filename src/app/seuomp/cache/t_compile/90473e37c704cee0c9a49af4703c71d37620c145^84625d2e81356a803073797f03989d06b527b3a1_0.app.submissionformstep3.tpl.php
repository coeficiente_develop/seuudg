<?php
/* Smarty version 3.1.39, created on 2022-03-18 14:13:52
  from 'app:submissionformstep3.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_623493a0e6f224_52421169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '84625d2e81356a803073797f03989d06b527b3a1' => 
    array (
      0 => 'app:submissionformstep3.tpl',
      1 => 1647574018,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'core:submission/form/step3.tpl' => 1,
  ),
),false)) {
function content_623493a0e6f224_52421169 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "additionalContributorsFields", null);?>
	<!--  Chapters -->
	<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', 'chaptersGridUrl', null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('router'=>(defined('ROUTE_COMPONENT') ? constant('ROUTE_COMPONENT') : null),'component'=>"grid.users.chapter.ChapterGridHandler",'op'=>"fetchGrid",'submissionId'=>$_smarty_tpl->tpl_vars['submissionId']->value,'publicationId'=>$_smarty_tpl->tpl_vars['publicationId']->value,'escape'=>false),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['load_url_in_div'][0], array( array('id'=>"chaptersGridContainer",'url'=>$_smarty_tpl->tpl_vars['chaptersGridUrl']->value),$_smarty_tpl ) );?>

<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>

<?php $_smarty_tpl->_subTemplateRender("core:submission/form/step3.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
