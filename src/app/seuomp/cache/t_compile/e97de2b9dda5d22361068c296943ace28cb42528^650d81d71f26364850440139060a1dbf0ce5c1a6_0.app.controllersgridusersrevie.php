<?php
/* Smarty version 3.1.39, created on 2022-03-18 15:11:14
  from 'app:controllersgridusersrevie' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6234a11232f347_92771289',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '650d81d71f26364850440139060a1dbf0ce5c1a6' => 
    array (
      0 => 'app:controllersgridusersrevie',
      1 => 1647574010,
      2 => 'app',
    ),
  ),
  'includes' => 
  array (
    'core:controllers/grid/users/reviewer/readReview.tpl' => 1,
  ),
),false)) {
function content_6234a11232f347_92771289 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "reviewerRecommendations", null);?>
	<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>

<?php if ($_smarty_tpl->tpl_vars['reviewAssignment']->value->getDateCompleted()) {?>
	<?php $_smarty_tpl->_assignInScope('reviewCompleted', true);
} else { ?>
	<?php $_smarty_tpl->_assignInScope('reviewCompleted', false);
}
echo '<script'; ?>
 type="text/javascript">
	$(function() {
		// Attach the form handler.
		$('#readReviewForm').pkpHandler('$.pkp.controllers.grid.users.reviewer.ReadReviewHandler', {
				reviewCompleted: <?php echo json_encode($_smarty_tpl->tpl_vars['reviewCompleted']->value);?>

		});
	});
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender("core:controllers/grid/users/reviewer/readReview.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
